<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'copyRightUrl' => 'http://www.girnarsoft.com',
    'copyRight' => 'GirnarSoft Pvt. Ltd',
    'imagesPath'=>'http://18.220.165.60/'
    // 'imagesPath'=>'http://localhost:5454/'
];
