<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mailed_reciepts".
 *
 * @property integer $id
 * @property integer $application_id
 * @property string $mailed_to_email
 * @property integer $mailed_by
 * @property string $content_html
 * @property string $mailed_at
 */
class MailedReciepts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mailed_reciepts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'mailed_to_email', 'content_html'], 'required'],
            [['application_id', 'mailed_by'], 'integer'],
            [['content_html'], 'string'],
            [['mailed_at'], 'safe'],
            [['mailed_to_email'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'mailed_to_email' => 'Mailed To Email',
            'mailed_by' => 'Mailed By',
            'content_html' => 'Content Html',
            'mailed_at' => 'Mailed At',
        ];
    }
}
