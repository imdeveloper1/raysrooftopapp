<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "application_services".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $application_id
 * @property string $service_id
 * @property double $price
 * @property string $status
 *
 * @property Service $service
 * @property User $user
 * @property Application $application
 */
class ApplicationServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'service_id', 'price'], 'required'],
            [['user_id', 'application_id', 'service_id'], 'integer'],
            [['price'], 'number'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'application_id' => 'Application ID',
            'service_id' => 'Service ID',
            'price' => 'Price',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(Application::className(), ['id' => 'application_id']);
    }
}
