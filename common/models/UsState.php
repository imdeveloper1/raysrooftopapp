<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "us_state".
 *
 * @property integer $id
 * @property string $name
 */
class UsState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'us_state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    public static function getStates()
    {
        return UsState::find()->all();
    }
}
