<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_images".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $status
 */
class BannerImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','description','type'], 'required'],
             ['image', 'required','on'=>'create'],
            [['status'], 'string'],
            [['title'], 'string', 'max' => 100],
            ['image', 'file', 'extensions' => 'jpg,png,jpeg'],
            //[['image'], 'string', 'max' => 200]
        ];
    }            

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'status' => 'Status',
            'type' => 'Type',
        ];
    }
}
