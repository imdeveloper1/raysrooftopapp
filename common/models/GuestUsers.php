<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "guest_users".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $email
 * @property string $phone_no
 * @property integer $created_by
 * @property string $created_at
 *
 * @property Application[] $applications
 * @property User $createdBy
 */
class GuestUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guest_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'email', 'phone_no'], 'required'],
            [['created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['fullname', 'email'], 'string', 'max' => 80],
            [['phone_no'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'email' => 'Email',
            'phone_no' => 'Phone No',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['guest_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
