<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Zipcodes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-warning">
    <div class="box-body">

<div class="zipcodes-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'zipcode')->fileInput() ?>

    <div class="form-group">
        <span style="color: red;">Note: CSV files must be less than 2MB in size. </span>
        <br />
        <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
        &nbsp;&nbsp;&nbsp;<a href="<?php echo Yii::getAlias('@web') . "/img/zipcode_sample.csv" ?>" style="text-decoration: underline;">Download Sample CSV File</a>
    </div>    

    <?php ActiveForm::end(); ?>

</div>
    </div>
</div>
