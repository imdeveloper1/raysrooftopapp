<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Zipcodes */

$this->title = 'Update Zipcodes: ' . ' ' . $model->zipcode;
$this->params['breadcrumbs'][] = ['label' => 'Zipcodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->city, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="zipcodes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
