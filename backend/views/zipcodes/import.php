<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Zipcodes */

$this->title = 'Import Zipcodes';
$this->params['breadcrumbs'][] = ['label' => 'Zipcodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zipcodes-import">    

    <?= $this->render('_import', [
        'model' => $model,
    ]) ?>

</div>
