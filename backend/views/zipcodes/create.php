<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Zipcodes */

$this->title = 'Create Zipcodes';
$this->params['breadcrumbs'][] = ['label' => 'Zipcodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zipcodes-create">

    <h1><?php // Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
