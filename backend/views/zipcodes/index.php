<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ZipcodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zipcodes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="zipcodes-form">
                
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Zipcodes', ['create'], ['class' => 'btn btn-success']) ?>&nbsp;&nbsp;
        <?= Html::a('Import CSV', ['import'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
             [
                'attribute' => 'city',
                'label' => 'Location',
             ],   
            'zipcode',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){

                    if($model->status=='1') {

                        return 'Active';
                    }else{

                        return 'Inactive';
                    }

                }
            ],
            //'created_date',
            // 'modified_date',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;&nbsp;{delete}',],
        ],
    ]); ?>

</div>
    </div>
</div>
