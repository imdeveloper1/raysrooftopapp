<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="email-template-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
            <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

            <?=
            $form->field($model, 'description')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ])
            ?>

            <?php // $form->field($model, 'status')->dropDownList([ '0', '1',], ['prompt' => '']) ?>

                <?php // $form->field($model, 'updated_at')->textInput()  ?>

            <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' =>'btn btn-success']) ?>
            </div>

<?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
