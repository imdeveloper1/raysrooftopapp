<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="email-template-index">            
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--            <p>
                <?php // Html::a('Create Email Template', ['create'], ['class' => 'btn btn-success']) ?>
            </p>-->

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'name',
                    'subject',
                    'description:ntext',
                    //'status',
                    // 'updated_at',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{update}'],
                ],
            ]);
            ?>

        </div>
    </div>
</div>
