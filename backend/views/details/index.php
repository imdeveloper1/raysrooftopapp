<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="details-index">

<!--    <h1><?php //echo Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?php //echo Html::a('Create Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'connection_type',
            'contract_load_value',
            //'contract_load_unit',
            'roof_type',
             'roof_inclination',
             'roof_area',
            // 'roof_area_unit',
            // 'monthly_unit',
            // 'monthly_bill',
            // 'roof_img1',
            // 'bill_img1',
            // 'roof_img2',
            // 'bill_img2',
            // 'roof_img3',
            // 'bill_img3',
            // 'roof_img4',
            // 'bill_img4',
            // 'roof_img5',
            // 'bill_img5',
            // 'roof_img6',
            // 'bill_img6',
            // 'roof_img7',
            // 'bill_img7',
            // 'roof_img8',
            // 'bill_img8',
            // 'system_size',
            // 'first_year_generation',
            // 'twentyfive_year_generation',
            // 'saving_per_month',
            // 'saving_twentyfive_year',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{my_button}&nbsp;&nbsp;{view}'],
        ],
    ]); ?>

</div>
</div>
</div>
