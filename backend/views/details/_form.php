<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Details */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'connection_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contract_load_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contract_load_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_inclination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_area')->textInput() ?>

    <?= $form->field($model, 'roof_area_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'monthly_unit')->textInput() ?>

    <?= $form->field($model, 'monthly_bill')->textInput() ?>

    <?= $form->field($model, 'roof_img1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'roof_img8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bill_img8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'system_size')->textInput() ?>

    <?= $form->field($model, 'first_year_generation')->textInput() ?>

    <?= $form->field($model, 'twentyfive_year_generation')->textInput() ?>

    <?= $form->field($model, 'saving_per_month')->textInput() ?>

    <?= $form->field($model, 'saving_twentyfive_year')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
