<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'connection_type') ?>

    <?= $form->field($model, 'contract_load_value') ?>

    <?= $form->field($model, 'contract_load_unit') ?>

    <?= $form->field($model, 'roof_type') ?>

    <?php // echo $form->field($model, 'roof_inclination') ?>

    <?php // echo $form->field($model, 'roof_area') ?>

    <?php // echo $form->field($model, 'roof_area_unit') ?>

    <?php // echo $form->field($model, 'monthly_unit') ?>

    <?php // echo $form->field($model, 'monthly_bill') ?>

    <?php // echo $form->field($model, 'roof_img1') ?>

    <?php // echo $form->field($model, 'bill_img1') ?>

    <?php // echo $form->field($model, 'roof_img2') ?>

    <?php // echo $form->field($model, 'bill_img2') ?>

    <?php // echo $form->field($model, 'roof_img3') ?>

    <?php // echo $form->field($model, 'bill_img3') ?>

    <?php // echo $form->field($model, 'roof_img4') ?>

    <?php // echo $form->field($model, 'bill_img4') ?>

    <?php // echo $form->field($model, 'roof_img5') ?>

    <?php // echo $form->field($model, 'bill_img5') ?>

    <?php // echo $form->field($model, 'roof_img6') ?>

    <?php // echo $form->field($model, 'bill_img6') ?>

    <?php // echo $form->field($model, 'roof_img7') ?>

    <?php // echo $form->field($model, 'bill_img7') ?>

    <?php // echo $form->field($model, 'roof_img8') ?>

    <?php // echo $form->field($model, 'bill_img8') ?>

    <?php // echo $form->field($model, 'system_size') ?>

    <?php // echo $form->field($model, 'first_year_generation') ?>

    <?php // echo $form->field($model, 'twentyfive_year_generation') ?>

    <?php // echo $form->field($model, 'saving_per_month') ?>

    <?php // echo $form->field($model, 'saving_twentyfive_year') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
