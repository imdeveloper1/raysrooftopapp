<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Details */

$this->title = 'Details View';
$this->params['breadcrumbs'][] = ['label' => 'Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Details View';
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="details-view">

<!--    <h1><?= Html::encode($this->title) ?></h1>-->

<!--    <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
    </p>-->

            <?=
            DetailView::widget([
            'model' => $model,
            'attributes' => [
            //'id',
            'connection_type',
            'contract_load_value',
            'contract_load_unit',
            'roof_type',
            'roof_inclination',
            'roof_area',
            'roof_area_unit',
            'monthly_unit',
            'monthly_bill',
//            'system_size',
//            'first_year_generation',
//            'twentyfive_year_generation',
//            'saving_per_month',
            //'saving_twentyfive_year',
            [
            'label' => 'Date',
            'value' => date("d M Y h:i A", $model->created_at),
            ],

                ],
            ])
            ?>

        </div>
    </div>
    <div class="box-body">
        <div class="row">            
            <div class="col-sm-12">
                <?php if(!empty($model->roof_img1)||!empty($model->roof_img2)||!empty($model->roof_img3)||!empty($model->roof_img4)||!empty($model->roof_img5)||!empty($model->roof_img6||!empty($model->roof_img7||!empty($model->roof_img8)))){ ?>
                <div class="bottom_img">
                <h2>Roof Image:</h2>
                <?php if($model->roof_img1){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img1; ?>" target="_blank"><img src="<?php echo $model->roof_img1; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img2){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img2; ?>" target="_blank"><img src="<?php echo $model->roof_img2; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img3){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img3; ?>" target="_blank"><img src="<?php echo $model->roof_img3; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img4){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img4; ?>" target="_blank"><img src="<?php echo $model->roof_img4; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img5){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img5; ?>" target="_blank"><img src="<?php echo $model->roof_img5; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img6){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img6; ?>" target="_blank"><img src="<?php echo $model->roof_img6; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img7){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img7; ?>" target="_blank"><img src="<?php echo $model->roof_img7; ?>" height="100" width="100"></a></div> <?php } ?>
                <?php if($model->roof_img8){ ?> <div class="img_pt"><a href="<?php echo $model->roof_img8; ?>" target="_blank"><img src="<?php echo $model->roof_img8; ?>" height="100" width="100"></a></div> <?php } ?>                
                </div>                
                <?php } if(!empty($model->bill_img1)||!empty($model->bill_img2)||!empty($model->bill_img3)||!empty($model->bill_img4)||!empty($model->bill_img5)||!empty($model->bill_img6||!empty($model->bill_img7||!empty($model->bill_img8)))){ ?>
                  <div class="bottom_img">
                      <h2>Bill Image: &nbsp;&nbsp;</h2>
                <?php if($model->bill_img1){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img1; ?>" target="_blank"><img src="<?php echo $model->bill_img1; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img2){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img2; ?>" target="_blank"><img src="<?php echo $model->bill_img2; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img3){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img3; ?>" target="_blank"><img src="<?php echo $model->bill_img3; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img4){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img4; ?>" target="_blank"><img src="<?php echo $model->bill_img4; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img5){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img5; ?>" target="_blank"><img src="<?php echo $model->bill_img5; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img6){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img6; ?>" target="_blank"><img src="<?php echo $model->bill_img6; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img7){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img7; ?>" target="_blank"><img src="<?php echo $model->bill_img7; ?>" height="100" width="100"></a></div> <?php } ?>                
                <?php if($model->bill_img8){ ?> <div class="img_pt"><a href="<?php echo $model->bill_img8; ?>" target="_blank"><img src="<?php echo $model->bill_img8; ?>" height="100" width="100"></a></div> <?php } ?>                
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
