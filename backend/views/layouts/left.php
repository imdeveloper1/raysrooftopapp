<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel user-logo">
            <div class="logo_img">
                <img src="<?= $directoryAsset ?>/img/logo.png" alt="User Image"/>
            </div>
<!--            <div class="pull-left info">
                <p><?php if (!Yii::$app->user->isGuest) echo Yii::$app->user->identity->username; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>-->
        </div>

        <style>
            .user-logo{text-align: center;}
            .user-logo .logo_img{text-align: center;}
             .user-logo .logo_img img{max-width: 100%;}
        </style>
        
        <!-- search form 
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
         /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        //['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                        //   ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                        ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/'], 'visible' => \common\models\User::isAdminOrStaff()],
                        ['label' => 'User manager', 'icon' => 'fa fa-user', 'url' => '#',
                            'items' => [
                                ['label' => 'Users', 'icon' => 'fa fa-circle-o', 'url' => ['/user/list-users'], 'visible' => \common\models\User::isAdmin()],
//                                ['label' => 'Web Users', 'icon' => 'fa fa-circle-o', 'url' => '#',
//                                    'items' => [
//                                        ['label' => 'Created By Staff', 'icon' => 'fa fa-circle-o', 'url' => ['/guest-users/index'], 'visible' => \common\models\User::isAdminOrStaff()],
//                                        ['label' => 'Guest Users', 'icon' => 'fa fa-circle-o', 'url' => ['/guest-users/list'], 'visible' => \common\models\User::isAdminOrStaff()],
//                                    ],
//                                ],
                                //['label' => 'Create Guest User', 'icon' => 'fa fa-circle-o', 'url' => ['/guest-users/create'], 'visible' => \common\models\User::isAdminOrStaff()],
                                //['label' => 'Create User', 'icon' => 'fa fa-circle-o', 'url' => ['user/create-user'],],
//                                ['label' => 'Staff Members', 'icon' => 'fa fa-circle-o', 'url' => ['/user/list-staff'], 'visible' => \common\models\User::isAdmin()],
//                                ['label' => 'Add Staff Member', 'icon' => 'fa fa-circle-o', 'url' => ['user/create-staff'], 'visible' => \common\models\User::isAdmin()],
                            ], 'visible' => \common\models\User::isAdminOrStaff()
                        ],
                        ['label' => 'Splash Screen', 'icon' => 'fa fa-file-image-o', 'url' => ['/banner-images'], 'visible' => \common\models\User::isAdmin()],
                        ['label' => 'Zipcode Manager', 'icon' => 'fa fa-area-chart', 'url' => ['/zipcodes'], 'visible' => \common\models\User::isAdmin()],
                        ['label' => 'Notifications', 'icon' => 'fa fa-bell', 'url' => ['/notifications/index'], 'visible' => \common\models\User::isAdmin()],
                        ['label' => 'Email Template', 'icon' => 'fa fa-envelope', 'url' => ['/email-template/index'], 'visible' => \common\models\User::isAdmin()],
                        ['label' => 'Search Details', 'icon' => 'fa fa-search', 'url' => ['/details/index'], 'visible' => \common\models\User::isAdmin()],
//                        ['label' => 'Content Manager', 'icon' => 'fa fa-dashboard', 'url' => ['/contents'], 'visible' => \common\models\User::isAdmin()],                        
//                        ['label' => 'Settings', 'icon' => 'fa fa-dashboard', 'url' => ['/settings'], 'visible' => \common\models\User::isAdmin()],
//                        ['label' => 'Change password', 'icon' => 'fa fa-dashboard', 'url' => ['user/changepassword'], 'visible' => \common\models\User::isAdminOrStaff()],
                    /* [
                      'label' => 'Same tools',
                      'icon' => 'fa fa-share',
                      'url' => '#',
                      'items' => [
                      ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                      ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                      [
                      'label' => 'Level One',
                      'icon' => 'fa fa-circle-o',
                      'url' => '#',
                      'items' => [
                      ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                      [
                      'label' => 'Level Two',
                      'icon' => 'fa fa-circle-o',
                      'url' => '#',
                      'items' => [
                      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                      ],
                      ],
                      ],
                      ],
                      ],
                      ], */
                    ],
                ]
        )
        ?>

    </section>

</aside>
