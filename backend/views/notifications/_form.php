<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Notifications */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="notifications-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Deactive', ], ['prompt' => 'Select Status']) ?>

            <?php // $form->field($model, 'created_at')->textInput() ?>

            <?php // $form->field($model, 'updated_at')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
