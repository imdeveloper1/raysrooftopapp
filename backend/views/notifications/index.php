<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\notificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="notifications-index">

<!--            <h1><?= Html::encode($this->title) ?></h1>-->
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--            <p>
                <?php //echo Html::a('Create Notifications', ['create'], ['class' => 'btn btn-success']) ?>
            </p>-->

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'zipcode',
                    'email:email',
                    //'status',
                    //'created_at',
                    // 'updated_at',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}'],
                ],
            ]);
            ?>

        </div>
    </div>
</div>
