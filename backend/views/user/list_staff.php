<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Staff Members';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-warning">
    <div class="box-body">
        <div class="user-form">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Staff Member', ['create-staff'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
         //   ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'username',
           // 'auth_key',
         //   'password',
            //'email:email',
            'fullname',
           // 'phone_no',

            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){

                    if($model->status=='1') {

                        return 'Active';
                    }else{

                        return 'Inactive';
                    }

                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{my_button}&nbsp;&nbsp;{update_staff}&nbsp;{delete}',
                'buttons' => [
                    'my_button' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-lock"></span>', ['/user/changepassword', 'id' => $model->id]);
                    },
                    'update_staff' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/user/update-staff', 'id' => $model->id]);
                    },
                ],
            ],
            // 'language',
            // 'status',
            // 'type',
            // 'created_at',
            // 'updated_at',

         //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
    </div>
</div>
