<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['list-users']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-warning">
    <div class="box-body">
        <div class="user-form">
            <?php
            $up = $model->status;
            switch ($up) {
                case 0:
                    $s_value = "Inactive";
                    break;
                case 1:
                    $s_value = 'Active';
                    break;
                case 2:
                    $s_value = "Approved";
                    break;
                case 3:
                    $s_value = "Block";
                    break;
                case 4:
                    $s_value = "Pending";
                    break;
            }
            ?>


            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?=
                Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
            </p>

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //     'id',
                    [
                        'attribute' => 'username',
                        'label' => 'Name',
                    ],
                    //  'auth_key',
                    //   'password',
                    'email:email',
                    //'fullname',
                    'phone_no',
                    'address',
                    //   'status',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => $s_value
                    ],
                //  'type',
                // 'created_at',
                // 'updated_at',
                ],
            ])
            ?>

        </div>
    </div>
</div>
