<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update';
$this->params['breadcrumbs'][] = ['label' => 'Staff Members', 'url' => ['list-staff']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">



    <?= $this->render('_staff_form', [
        'model' => $model,
    ]) ?>

</div>
