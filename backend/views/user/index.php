<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-warning">
    <div class="box-body">
        <div class="user-form">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create User', ['create-user'], ['class' => 'btn btn-success']) ?>
            </p>

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //   ['class' => 'yii\grid\SerialColumn'],
                    //  'id',
                    [
                        'attribute' => 'username',
                        'label' => 'Name',
                    ],
                    // 'auth_key',
                    //   'password',
                    'email:email',
                    //'fullname',
                    'phone_no',
                    'address',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function($model) {

                            switch ($model->status) {
                                case 0:
                                    return "Inactive";
                                    break;
                                case 1:
                                    return 'Active';
                                    break;
                                case 2:
                                    return "Approved";
                                    break;
                                case 3:
                                    return "Block";
                                    break;
                                case 4:
                                    return "Pending";
                                    break;
                            }                          
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{my_button}&nbsp;&nbsp;{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                        'buttons' => [
                            'my_button' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-lock"></span>', ['/user/changepassword', 'id' => $model->id]);
                            },
                                ],
                            ],
                        // 'language',
                        // 'status',
                        // 'type',
                        // 'created_at',
                        // 'updated_at',
                        //  ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]);
                    ?>

        </div>
    </div>
</div>
