<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\User */
?>
<div class="box box-warning">
    <div class="box-body">

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


    <?php  if($this->title=='Create Staff Member'){  ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?php }  ?>
    <!--
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    -->
    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
    </div>
</div>
