<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\user */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-warning">
    <div class="box-body">

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label('Name') ?>


<?php  if($this->title=='Create'){  ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?php }  ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_no')->textInput() ?>
    
    <?= $form->field($model, 'address')->textarea() ?>



    <?= $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Deactive', '2' => 'Approved', '3' => 'Block', '4' => 'Pending'], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' =>'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
    </div>
</div>
