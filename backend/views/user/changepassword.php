<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\user */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Change password';
//$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-warning">
    <div class="box-body">

        <div class="user-form">

            <?php $form = ActiveForm::begin(); ?>


            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

            <?php if(Yii::$app->session->hasFlash('old_pa')) { ?>
                <div class="errorMessage">
                    <span style="color:red;"><?php   echo Yii::$app->session->getFlash('old_pa'); ?></span>
                </div>
            <?php } ?>


            <?= $form->field($model, 'newpassword')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'con_password')->passwordInput(['maxlength' => true]) ?>




            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'change password' : 'change password', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
