<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BannerImages */

$this->title = 'Add Splash Screen';
$this->params['breadcrumbs'][] = ['label' => 'Splash Screen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-images-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
