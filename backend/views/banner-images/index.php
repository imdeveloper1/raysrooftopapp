<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BannerImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Splash Screen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="banner-images-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?php echo Html::a('Add Splash Screen', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'title',
                    'description',
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'value' => function($model) {

                            if ($model->type == '2') {
                                return 'Splash Screen';
                            } else {
                                return 'Slider';
                            }
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function($model) {

                            if ($model->status == '1') {
                                return 'Active';
                            } else {
                                return 'Inactive';
                            }
                        }
                    ],
                    [
                        'attribute' => 'image',
                        'format' => 'html',
                        'value' => function ($data) {
                            $access_params = yii::$app->params;
                            return Html::img($access_params['imagesPath'] .'backend/web'. $data->image, ['alt' => 'Banner', 'class' => 'img-thumbnail', 'width' => '100px', 'height' => '80px']);
                        },
                            ],
                            [
                                'attribute' => 'logo',
                                'format' => 'html',
                                'value' => function ($data) {
                                    $access_params = yii::$app->params;
                                    if($data->type==1){
                                    return Html::img($access_params['imagesPath'] .'backend/web'. $data->logo, ['alt' => '',  'width' => '100px', 'height' => '80px']);
                                    } else {
                                        return '';
                                    }
                                }],
                                    ['class' => 'yii\grid\ActionColumn',
                                        'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}'],
                                ],
                            ]);
                            ?>

        </div>
    </div>
</div>