<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\BannerImages */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-warning">
    <div class="box-body">
        <div class="banner-images-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?php            
            if ($model->isNewRecord) {
                echo $form->field($model, 'type')->dropDownList([ 1 => 'Slider', 2 => 'Splash Screen',], ['prompt' => 'Select type', 'onchange' => 'fun(this.value)']);
            }
            ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?php
//            $form->field($model, 'description')->widget(CKEditor::className(), [
//                'options' => ['rows' => 6],
//                'preset' => 'basic'
//            ])
                    
                    echo $form->field($model, 'description')->textarea();
            ?>
            <?php $img_text = 'Splash Screen'; if($model->type=='1') { $img_text = 'Background Image'; } else if($model->type=='2') { $img_text = 'Splash Screen'; }?>
            <?= $form->field($model, 'image')->fileInput()->label($img_text); ?>
            <?php
            if ($model->image) {
                echo '<img class="img-thumbnail" src="' . yii::$app->params['imagesPath'] .'backend/web'. $model->image . '" width="100" height="80" alt="Banner">';
            }
            ?>
            <div id="logo_id" <?php if($model->type!='1') { ?> style="display: none;" <?php } ?>>
                <?= $form->field($model, 'logo')->fileInput() ?>
                <?php
                if ($model->logo) {
                    echo '<img class="img-thumbnail" src="' . yii::$app->params['imagesPath'] .'backend/web'. $model->logo . '" width="100" height="80" alt="Banner">';
                }
                ?>
            </div>
            <?php
            if (!$model->isNewRecord) {
                echo $form->field($model, 'status')->dropDownList([ 1 => 'Active', 0 => 'Deactive',], ['prompt' => 'Select Status']);
            }            
            ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<script>
    function fun(val) {
        if (val ==1) {            
            $('#logo_id').show();
            $("label[for*='bannerimages-image']").html("Background Image");                     
        } else {
            $('#logo_id').hide();
            $("label[for*='bannerimages-image']").html("Splash Screen");            
        }
    }
</script>