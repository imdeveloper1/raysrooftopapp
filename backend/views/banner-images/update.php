<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BannerImages */

$this->title = 'Update Splash Screen: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Splash Screen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="banner-images-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
