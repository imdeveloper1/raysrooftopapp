<?php

namespace backend\controllers;

use Yii;
use backend\models\Zipcodes;
use backend\models\ZipcodesSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZipcodesController implements the CRUD actions for Zipcodes model.
 */
class ZipcodesController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zipcodes models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ZipcodesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zipcodes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zipcodes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Zipcodes();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            $model->updated_at = time();
            if ($model->save())
                return $this->redirect(['index']);
            else
                return $this->render('create', ['model' => $model]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Zipcodes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = time();
            if ($model->save())
                return $this->redirect(['index']);
            else
                return $this->render('update', ['model' => $model]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Zipcodes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zipcodes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zipcodes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Zipcodes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport() {
        $model = new Zipcodes();        
        if ($model->load(Yii::$app->request->post())) {           
            $model->zipcode = UploadedFile::getInstance($model, 'zipcode');  
            if ($model->zipcode->size < 2036740 && $model->zipcode->size!=0) {
                $ext = substr(strtolower(strrchr($model->zipcode->name, '.')), 1);
                if (trim($ext) == 'csv') {
                    ini_set('memory_limit', '-1');
                    if ($model->zipcode) {
                        $file_handle = fopen($model->zipcode->tempName, "r");
                        $i = 1;
                        $products = array();
                        while (!feof($file_handle)) {
                            $data = fgetcsv($file_handle, 0, ',');
                            if ($i != 1) {
                                if (!empty($data)) {
                                    $products[] = ['city' => $data[0], 'zipcode' => trim($data[1]), 'status' => 1];
                                }
                            }
                            $i++;
                        }
                        fclose($file_handle);
                        $columnNameArray = ['city', 'zipcode', 'status'];
                        if (count($products) > 0) {
                            Yii::$app->db->createCommand()->batchInsert('zipcodes', $columnNameArray, $products)->execute();
                            Yii::$app->session->setFlash('success', 'Zipcode has been saved.');
                            return $this->redirect(['index']);
                        }



//                    $handle = fopen($model->zipcode->tempName, "r");
//                    $i = 1;
//                    $count = 0;
//                    $notcount = 0;
//                    while (($fileop = fgetcsv($handle, 1000, ",")) !== false) {
//                        if ($i != 1) {
//                            $city = $fileop[0];
//                            $zipcode = $fileop[1];
//                            $model = new Zipcodes();
//                            $model->zipcode = $zipcode;
//                            $model->city = $city;
//                            $model->status = 1;
//                            $model->created_at = time();
//                            $model->updated_at = time();
//                            $query = $model->save();
//                            if ($query)
//                                $count = $count + 1;
//                            else
//                                $notcount = $notcount + 1;
//                        }
//                        $i++;
//                    }
//                    if ($count) {
//                        if ($count > 1)
//                            $message = 'total added records are ' . $count;
//                        else
//                            $message = 'total added record is ' . $count;
//
//                        if ($notcount > 1)
//                            $message2 = ' total exists records are ' . $notcount;
//                        else
//                            $message2 = ' total exists record is ' . $notcount;
//
//                        Yii::$app->session->setFlash('success', 'Zipcode has been saved. ' . $message . $message2);
//                        return $this->redirect(['index']);
//                    } else {
//                        if ($notcount > 1)
//                            $message2 = 'total exists records are ' . $notcount;
//                        else
//                            $message2 = 'total exists record is ' . $notcount;
//                        Yii::$app->session->setFlash('error', 'Zipcode records are allready exists . ' . $message2);
//                        return $this->redirect('import', ['model' => $model]);
//                    }
                    } else {
                        Yii::$app->session->setFlash('error', 'file not attached. Please, try again.');
                        return $this->redirect('import', ['model' => $model]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', ucwords($ext) . ' file format not supported. Please, try again.');
                    return $this->redirect('import', ['model' => $model]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, your file is too large.');
                return $this->redirect('import', ['model' => $model]);
            }
        } else {
            return $this->render('import', [
                        'model' => $model,
            ]);
        }
    }

}
