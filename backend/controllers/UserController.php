<?php

namespace backend\controllers;

use Yii;
use backend\models\user;
use backend\models\Staff;
use backend\models\change;
use backend\models\SearchUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for user model.
 */
class UserController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all user models.
     * @return mixed
     */
    public function actionIndex() {
        $this->redirect(['/']);
    }

    public function actionListUsers() {
        $searchModel = new SearchUser(['type' => 'user']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListStaff() {
        $searchModel = new SearchUser(['type' => 'staff']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list_staff', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChangepassword($id = 'null') {

        if ($id == 'null') {
            $id = Yii::$app->user->identity->id;
        } else {
            $id = $id;
        }

        $model = new change();

        if ($model->load(Yii::$app->request->post())) {
            $models = change::findOne($id);
            $oldpass = $models->password;
            //echo "<pre>"; print_r($oldpass);die;
            $pass = Yii::$app->request->post();
            $new_pass = md5($pass['change']['password']);
            //  echo "<pre>"; print_r($pass['change']['password']);die;
            if ($new_pass == $oldpass) {
                $password = md5($pass['change']['newpassword']);
                $models->password = $password;
                $models->newpassword = '1';
                $models->con_password = '1';



                if ($models->save()) {
                    $old_pa = 'Password has been changed successfully';
                    Yii::$app->session->setFlash('old_pa', $old_pa);
                    return $this->goHome();
                } else {
                    print_r($models->getErrors());
                }
            } else {

                $old_pa = 'Your old password is wrong So please should be correct password';
                Yii::$app->session->setFlash('old_pa', $old_pa);
                return $this->redirect(['changepassword?id=' . $id]);
            }
        } else {


            return $this->render('changepassword', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single user model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new user model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateUser() {
        $model = new user();
        if ($model->load(Yii::$app->request->post())) {
            $pass = Yii::$app->request->post();
            $passwo = $pass['user']['password'];
            $passw = md5($passwo);
            $model->password = $passw;
            $model->type = 'user';
            $status = $model->save();
            if ($status) {                
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'The user could not be saved. Please, try again.');
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateStaff() {
        $model = new Staff();

        if ($model->load(Yii::$app->request->post())) {
            $pass = Yii::$app->request->post();
            $passwo = $pass['Staff']['password'];
            $passw = md5($passwo);
            $model->password = $passw;
            $model->type = 'staff';
            $model->save();
            return $this->redirect(['list-staff', 'id' => $model->id]);
        } else {
            return $this->render('create-staff', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing user model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdateStaff($id) {
        $model = Staff::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['list-staff', 'id' => $model->id]);
        } else {
            return $this->render('update-staff', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing user model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['list-users']);
    }

    /**
     * Finds the user model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return user the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = user::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
