<?php

namespace backend\controllers;

use Yii;
use common\models\BannerImages;
use common\models\BannerImagesSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BannerImagesController implements the CRUD actions for BannerImages model.
 */
class BannerImagesController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all BannerImages models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BannerImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BannerImages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BannerImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new BannerImages();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile_pic = UploadedFile::getInstance($model, 'image');
            $logo_pic = UploadedFile::getInstance($model, 'logo');
            $file_picture = '/banner_images/' . time() . "_" . str_replace(' ', '_', $uploadedFile_pic->name);
            $uploadPath_picture = Yii::getAlias('@backend') . "/web" . $file_picture;
            $uploadedFile_pic->saveAs($uploadPath_picture);
            $model->image = $file_picture;
            if (!empty($logo_pic)) {
                $logo_picture = '/banner_images/' . time() . "_" . str_replace(' ', '_', $logo_pic->name);
                $uploadPath_logo = Yii::getAlias('@backend') . "/web" . $logo_picture;
                $logo_pic->saveAs($uploadPath_logo);
                $model->logo = $logo_picture;
            }
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BannerImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $file_picture = $model->image;
        $type = $model->type;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile_pic = UploadedFile::getInstance($model, 'image');
            $logo_pic = UploadedFile::getInstance($model, 'logo');
            if ($uploadedFile_pic) {
                $file_picture = '/banner_images/' . time() . "_" . str_replace(' ', '_', $uploadedFile_pic->name);
                $uploadPath_picture = Yii::getAlias('@backend') . "/web" . $file_picture;
                $uploadedFile_pic->saveAs($uploadPath_picture);
            }
            if ($type == 2 && $model->status == 1) {
                $posts = Yii::$app->db->createCommand('UPDATE banner_images SET status=0 WHERE type=2 and id!='.$id)
                        ->execute();
            }
            
            $model->image = $file_picture;
            if (!empty($logo_pic)) {
                $logo_picture = '/banner_images/' . time() . "_" . str_replace(' ', '_', $logo_pic->name);
                $uploadPath_logo = Yii::getAlias('@backend') . "/web" . $logo_picture;
                $logo_pic->saveAs($uploadPath_logo);
                $model->logo = $logo_picture;
            }                
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BannerImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BannerImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannerImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BannerImages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
