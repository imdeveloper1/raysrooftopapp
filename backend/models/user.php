<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $email
 * @property string $fullname
 * @property string $phone_no
 * @property string $language
 * @property integer $status
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class user extends \yii\db\ActiveRecord
{

public $newpassword;
    public $con_password;
    /**
     * @inheritdoc
     */
   // const STATUS_DELETED = 0;
   // const STATUS_ACTIVE = 10;
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email','password', 'address', 'phone_no','status'], 'required'],
            [['status','created_at', 'updated_at'], 'integer'],

            ['phone_no','match', 'pattern' => '/^[0-9]{1,10}$/', 'message' => 'Phone no. must be a number of maximum 10 character long'],
            [['address'], 'string'],
            ['password','required','on' => 'create'],
            ['address',
            'match', 'not' => true, 'pattern' => '[a-zA-Z0-9\s]',
            'message' => 'Invalid characters in address'],
            ['password', 'string', 'min' => 6],
            ['phone_no', 'string', 'min' => 10,'max' => 10],
            [['username', 'password', 'email', 'address', 'language'], 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'unique'],
          //  ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
          //  'auth_key' => 'Auth Key',
            'password' => 'Password',
            'email' => 'Email',            
            'phone_no' => 'Phone No',
            'address' => 'Address',
            'language' => 'Language',
//            /'status' => 'Status',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
