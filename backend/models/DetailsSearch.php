<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Details;

/**
 * DetailsSearch represents the model behind the search form about `backend\models\Details`.
 */
class DetailsSearch extends Details
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'roof_area', 'monthly_unit', 'monthly_bill', 'created_at', 'updated_at'], 'integer'],
            [['connection_type', 'contract_load_value', 'contract_load_unit', 'roof_type', 'roof_inclination', 'roof_area_unit', 'roof_img1', 'bill_img1', 'roof_img2', 'bill_img2', 'roof_img3', 'bill_img3', 'roof_img4', 'bill_img4', 'roof_img5', 'bill_img5', 'roof_img6', 'bill_img6', 'roof_img7', 'bill_img7', 'roof_img8', 'bill_img8', 'status'], 'safe'],
            [['system_size', 'first_year_generation', 'twentyfive_year_generation', 'saving_per_month', 'saving_twentyfive_year'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Details::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'roof_area' => $this->roof_area,
            'monthly_unit' => $this->monthly_unit,
            'monthly_bill' => $this->monthly_bill,
            'system_size' => $this->system_size,
            'first_year_generation' => $this->first_year_generation,
            'twentyfive_year_generation' => $this->twentyfive_year_generation,
            'saving_per_month' => $this->saving_per_month,
            'saving_twentyfive_year' => $this->saving_twentyfive_year,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'connection_type', $this->connection_type])
            ->andFilterWhere(['like', 'contract_load_value', $this->contract_load_value])
            ->andFilterWhere(['like', 'contract_load_unit', $this->contract_load_unit])
            ->andFilterWhere(['like', 'roof_type', $this->roof_type])
            ->andFilterWhere(['like', 'roof_inclination', $this->roof_inclination])
            ->andFilterWhere(['like', 'roof_area_unit', $this->roof_area_unit])
            ->andFilterWhere(['like', 'roof_img1', $this->roof_img1])
            ->andFilterWhere(['like', 'bill_img1', $this->bill_img1])
            ->andFilterWhere(['like', 'roof_img2', $this->roof_img2])
            ->andFilterWhere(['like', 'bill_img2', $this->bill_img2])
            ->andFilterWhere(['like', 'roof_img3', $this->roof_img3])
            ->andFilterWhere(['like', 'bill_img3', $this->bill_img3])
            ->andFilterWhere(['like', 'roof_img4', $this->roof_img4])
            ->andFilterWhere(['like', 'bill_img4', $this->bill_img4])
            ->andFilterWhere(['like', 'roof_img5', $this->roof_img5])
            ->andFilterWhere(['like', 'bill_img5', $this->bill_img5])
            ->andFilterWhere(['like', 'roof_img6', $this->roof_img6])
            ->andFilterWhere(['like', 'bill_img6', $this->bill_img6])
            ->andFilterWhere(['like', 'roof_img7', $this->roof_img7])
            ->andFilterWhere(['like', 'bill_img7', $this->bill_img7])
            ->andFilterWhere(['like', 'roof_img8', $this->roof_img8])
            ->andFilterWhere(['like', 'bill_img8', $this->bill_img8])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
