<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "email_template".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property integer $updated_at
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'subject', 'description', 'status', 'updated_at'], 'required'],
            [['description', 'status'], 'string'],
            [['updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'description' => 'Description',
            'status' => 'Status',
            'updated_at' => 'Updated At',
        ];
    }
}
