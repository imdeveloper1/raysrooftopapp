<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $email
 * @property string $fullname
 * @property string $phone_no
 * @property string $language
 * @property integer $status
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class Staff extends \yii\db\ActiveRecord
{

    public $newpassword;
    public $con_password;
    /**
     * @inheritdoc
     */
   // const STATUS_DELETED = 0;
   // const STATUS_ACTIVE = 10;
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','password', 'fullname'], 'required'],
            [['status','created_at', 'updated_at'], 'integer'],

            ['phone_no','match', 'pattern' => '/^[0-9]{1,10}$/', 'message' => 'Phone no. must be a number of maximum 10 character long'],
            [['fullname'], 'string'],
            ['password','required','on' => 'create'],
            ['fullname',
            'match', 'not' => true, 'pattern' => '/[^a-zA-Z_ -]/',
            'message' => 'Invalid characters in fullname'],
            ['password', 'string', 'min' => 6],
            [['username', 'password', 'email', 'fullname', 'language'], 'string', 'max' => 255],
            ['email', 'email'],
            [['email', 'username'], 'unique'],
          //  ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
          //  'auth_key' => 'Auth Key',
            'password' => 'Password',
            'email' => 'Email',
            'fullname' => 'Full Name',
            'phone_no' => 'Phone No',
            'language' => 'Language',
//            /'status' => 'Status',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
