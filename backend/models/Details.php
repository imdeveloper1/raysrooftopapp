<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "details".
 *
 * @property integer $id
 * @property string $connection_type
 * @property string $contract_load_value
 * @property string $contract_load_unit
 * @property string $roof_type
 * @property string $roof_inclination
 * @property integer $roof_area
 * @property string $roof_area_unit
 * @property integer $monthly_unit
 * @property integer $monthly_bill
 * @property string $roof_img1
 * @property string $bill_img1
 * @property string $roof_img2
 * @property string $bill_img2
 * @property string $roof_img3
 * @property string $bill_img3
 * @property string $roof_img4
 * @property string $bill_img4
 * @property string $roof_img5
 * @property string $bill_img5
 * @property string $roof_img6
 * @property string $bill_img6
 * @property string $roof_img7
 * @property string $bill_img7
 * @property string $roof_img8
 * @property string $bill_img8
 * @property double $system_size
 * @property double $first_year_generation
 * @property double $twentyfive_year_generation
 * @property double $saving_per_month
 * @property double $saving_twentyfive_year
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Details extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['connection_type', 'contract_load_value', 'contract_load_unit', 'roof_type', 'roof_inclination', 'roof_area', 'roof_area_unit', 'monthly_unit', 'monthly_bill', 'roof_img1', 'bill_img1', 'roof_img2', 'bill_img2', 'roof_img3', 'bill_img3', 'roof_img4', 'bill_img4', 'roof_img5', 'bill_img5', 'roof_img6', 'bill_img6', 'roof_img7', 'bill_img7', 'roof_img8', 'bill_img8', 'system_size', 'first_year_generation', 'twentyfive_year_generation', 'saving_per_month', 'saving_twentyfive_year', 'created_at', 'updated_at'], 'required'],
            [['roof_area', 'monthly_unit', 'monthly_bill', 'created_at', 'updated_at'], 'integer'],
            [['system_size', 'first_year_generation', 'twentyfive_year_generation', 'saving_per_month', 'saving_twentyfive_year'], 'number'],
            [['status'], 'string'],
            [['connection_type', 'contract_load_value', 'roof_type', 'roof_inclination', 'roof_img1', 'bill_img1'], 'string', 'max' => 180],
            [['contract_load_unit'], 'string', 'max' => 3],
            [['roof_area_unit'], 'string', 'max' => 40],
            [['roof_img2', 'bill_img2', 'roof_img3', 'bill_img3', 'roof_img4', 'bill_img4', 'roof_img5', 'bill_img5', 'roof_img6', 'bill_img6', 'roof_img7', 'bill_img7', 'roof_img8', 'bill_img8'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'connection_type' => 'Connection Type',
            'contract_load_value' => 'Contract Load Value',
            'contract_load_unit' => 'Contract Load Unit',
            'roof_type' => 'Roof Type',
            'roof_inclination' => 'Roof Inclination',
            'roof_area' => 'Roof Area',
            'roof_area_unit' => 'Roof Area Unit',
            'monthly_unit' => 'Monthly Unit',
            'monthly_bill' => 'Monthly Bill',
            'roof_img1' => 'Roof Img1',
            'bill_img1' => 'Bill Img1',
            'roof_img2' => 'Roof Img2',
            'bill_img2' => 'Bill Img2',
            'roof_img3' => 'Roof Img3',
            'bill_img3' => 'Bill Img3',
            'roof_img4' => 'Roof Img4',
            'bill_img4' => 'Bill Img4',
            'roof_img5' => 'Roof Img5',
            'bill_img5' => 'Bill Img5',
            'roof_img6' => 'Roof Img6',
            'bill_img6' => 'Bill Img6',
            'roof_img7' => 'Roof Img7',
            'bill_img7' => 'Bill Img7',
            'roof_img8' => 'Roof Img8',
            'bill_img8' => 'Bill Img8',
            'system_size' => 'System Size',
            'first_year_generation' => 'First Year Generation',
            'twentyfive_year_generation' => 'Twentyfive Year Generation',
            'saving_per_month' => 'Saving Per Month',
            'saving_twentyfive_year' => 'Saving Twentyfive Year',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
