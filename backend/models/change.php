<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $email
 * @property string $fullname
 * @property string $phone_no
 * @property string $language
 * @property integer $status
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class change extends \yii\db\ActiveRecord
{

    public $newpassword;
    public $con_password;
    /**
     * @inheritdoc
     */
    // const STATUS_DELETED = 0;
    // const STATUS_ACTIVE = 10;
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password','newpassword','con_password'], 'required'],
            ['con_password', 'compare', 'compareAttribute' => 'newpassword', 'message' => "Confirm Passwords don't match to New Password "],
          //  ['password', 'validatePassword'],

            //  ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }
    public function validatePassword($attribute, $params)
    {
      // echo  $this->id;

               // $this->addError($attribute, 'Incorrect username or password.');


    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',

            'password' => 'Old Password',
            'newpassword' => 'New Password',
            'con_password' => 'Confirm Password',

        ];
    }
}
