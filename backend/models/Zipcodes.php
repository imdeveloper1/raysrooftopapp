<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "zipcodes".
 *
 * @property integer $id
 * @property string $city
 * @property string $zipcode
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Zipcodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zipcodes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'zipcode', 'status'], 'required'],
            [['status','created_at', 'updated_at'], 'integer'],
            [['city'], 'string', 'max' => 180],
            [['zipcode'], 'string', 'max' => 180],
            [['city', 'zipcode'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'zipcode' => 'Zipcode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
