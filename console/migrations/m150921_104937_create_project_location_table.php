<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_104937_create_project_location_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `project_location` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `city_id` int(11) unsigned NOT NULL,
                          `project_id` int(11) unsigned NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_project_location_city_id_idx` (`city_id`),
                          KEY `fk_project_location_project_id_idx` (`project_id`),
                          CONSTRAINT `fk_project_location_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                          CONSTRAINT `fk_project_location_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150921_104937_create_project_location_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
