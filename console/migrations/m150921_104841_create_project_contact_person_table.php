<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_104841_create_project_contact_person_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `project_contact_person` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(50) NOT NULL,
                          `designation` varchar(100) NOT NULL,
                          `mobile` int(11) NOT NULL,
                          `email` varchar(150) NOT NULL,
                          `location` varchar(250) DEFAULT NULL,
                          `project_id` int(11) unsigned NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_project_contact_person_project_id_idx` (`project_id`),
                          CONSTRAINT `fk_project_contact_person_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150921_104841_create_project_contact_person_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
