<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_063253_create_industry_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `industry` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(150) NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150918_100354_create_industry_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
