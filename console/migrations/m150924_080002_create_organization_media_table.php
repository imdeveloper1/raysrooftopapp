<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_080002_create_organization_media_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `organization_media` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `path` varchar(150) NOT NULL,
                          `organization_id` int(11) unsigned NOT NULL,
                          `type` enum('image','video','document') NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_organization_id_idx` (`organization_id`),
                          KEY `fk_org_id_idx` (`organization_id`),
                          CONSTRAINT `fk_org_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

    }

    public function down()
    {
        echo "m150924_080002_create_organization_media_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
