<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_100355_create_user_profile_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `user_profile` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `first_name` varchar(150) NOT NULL,
                          `last_name` varchar(150) NOT NULL,
                          `address` varchar(450) NOT NULL,
                          `zipcode` int(6) NOT NULL,
                          `state_id` int(11) unsigned DEFAULT NULL,
                          `city_id` int(11) unsigned DEFAULT NULL,
                          `mobile` int(11) NOT NULL,
                          `user_id` int(11) NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_state_id_idx` (`state_id`),
                          KEY `fk_city_id_idx` (`city_id`),
                          KEY `fk_user_id_idx` (`user_id`),
                          CONSTRAINT `fk_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                          CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                          CONSTRAINT `fk_user_state_id` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150917_061937_create_user_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
