<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_070458_create_organization_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `organization` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(150) NOT NULL,
                          `type` enum('ngo','corporate','government') NOT NULL,
                          `description` text,
                          `logo` varchar(250) DEFAULT NULL,
                          `website` varchar(50) DEFAULT NULL,
                          `establish_year` int(4) DEFAULT NULL,
                          `employee` int(11) DEFAULT NULL,
                          `industry_id` int(11) unsigned NOT NULL,
                          `tin` varchar(20) DEFAULT NULL,
                          `cin` varchar(20) DEFAULT NULL,
                          `turn_over` bigint(20) DEFAULT NULL,
                          `net_worth` bigint(20) DEFAULT NULL,
                          `csr_amount` bigint(20) DEFAULT NULL,
                          `user_id` int(11) NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_industry_id_idx` (`industry_id`),
                          KEY `fk_user_id_idx` (`user_id`),
                          CONSTRAINT `fk_industry_id` FOREIGN KEY (`industry_id`) REFERENCES `industry` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                          CONSTRAINT `fk_org_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
    }

    public function down()
    {
        echo "m150918_070458_create_organization_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
