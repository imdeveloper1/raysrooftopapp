<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_104330_create_project_expenditure_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `project_expenditure` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `type` enum('capital','operating') DEFAULT NULL,
                          `sub_type` enum('building','equipment','others') DEFAULT NULL COMMENT 'Project Expenditure type building and equipment is fixed user can add multiple other types as well',
                          `amount` bigint(20) NOT NULL,
                          `project_id` int(11) unsigned NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
    }

    public function down()
    {
        echo "m150929_104330_create_project_expenditure_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
