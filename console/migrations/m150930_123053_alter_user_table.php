<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_123053_alter_user_table extends Migration
{
    public function up()
    {
        $this->execute("alter table user add column type enum('ngo', 'corporate', 'government', 'individual') after status;");
    }

    public function down()
    {
        echo "m150930_123053_alter_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
