<?php

use yii\db\Schema;
use yii\db\Migration;

class m150923_055800_alter_organization_table extends Migration
{
    public function up()
    {
        $this->execute('alter table organization add column registration_number varchar(50) after establish_year, add column registration_date int(11) after registration_number; ');
    }

    public function down()
    {
        echo "m150923_055800_alter_organization_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
