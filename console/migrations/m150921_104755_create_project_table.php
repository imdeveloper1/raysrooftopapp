<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_104755_create_project_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `project` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `title` varchar(250) NOT NULL,
                          `description` text,
                          `start_date` int(11) NOT NULL,
                          `end_date` int(11) NOT NULL,
                          `schedule_id` int(11) unsigned NOT NULL,
                          `goal_of_project` text,
                          `user_id` int(11) NOT NULL,
                          `category_id` int(11) unsigned NOT NULL,
                          `sub_category_id` int(11) unsigned NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_project_organization_id_idx` (`user_id`),
                          KEY `fk_project_category_id_idx` (`category_id`),
                          KEY `fk_project_sub_category_id_idx` (`sub_category_id`),
                          CONSTRAINT `fk_project_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                          CONSTRAINT `fk_project_sub_category_id` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                          CONSTRAINT `fk_project_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150921_104755_create_project_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
