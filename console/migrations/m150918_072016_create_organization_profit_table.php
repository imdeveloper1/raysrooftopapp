<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_072016_create_organization_profit_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `organization_profit` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `year` int(4) NOT NULL,
                          `profit` bigint(20) NOT NULL,
                          `organization_id` int(11) unsigned NOT NULL,
                          `created_at` int(11) DEFAULT NULL,
                          `updated_at` int(11) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_organization_id_idx` (`organization_id`),
                          CONSTRAINT `fk_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150918_072016_create_organization_profit_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
