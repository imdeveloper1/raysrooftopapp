<?php

use yii\db\Schema;
use yii\db\Migration;

class m151008_090135_update_user_profile_table extends Migration
{
    public function up()
    {
        $this->execute('alter table user_profile change column mobile mobile varchar(11) not null;');
    }

    public function down()
    {
        echo "m151008_090135_update_user_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
