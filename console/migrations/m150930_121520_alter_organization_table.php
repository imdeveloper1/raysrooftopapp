<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_121520_alter_organization_table extends Migration
{
    public function up()
    {
        $this->execute("alter table organization add column address varchar(250) not null, change column type type enum('corporate', 'partnership', 'sole_proprietorship');");
    }

    public function down()
    {
        echo "m150930_121520_alter_organization_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
