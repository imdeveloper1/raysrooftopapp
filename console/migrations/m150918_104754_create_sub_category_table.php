<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_104754_create_sub_category_table extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `sub_category` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(150) NOT NULL,
                          `category_id` int(11) unsigned NOT NULL,
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_category_id_idx` (`category_id`),
                          CONSTRAINT `fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        echo "m150918_104754_create_sub_category_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
