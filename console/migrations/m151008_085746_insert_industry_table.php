<?php

use yii\db\Schema;
use yii\db\Migration;

class m151008_085746_insert_industry_table extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `industry` VALUES (2,'loa group',0,1443521664),(4,'softwares',1443446311,1443446315),(5,'Select Industry',0,0),(6,'Advertising Industry',0,0),(7,'Agricultural Industry',0,0),(8,'Aluminium Industry',0,0),(9,'Automobile Industry',0,0),(10,'Aviation Industry',0,0),(11,'Banking Industry',0,0),(12,'Biotechnology Industry',0,0),(13,'Biscuit Industry',0,0),(14,'Cement Industry',0,0),(15,'Chocolate Industry',0,0),(16,'Coir Industry',0,0),(17,'Construction Industry',0,0),(18,'Copper Industry',0,0),(19,'Cosmetic Industry',0,0),(20,'Cottage Industry',0,0),(21,'Cotton Industry',0,0),(22,'Dairy Industry',0,0),(23,'Diamond Industry',0,0),(24,'Electronic Industry',0,0),(25,'Fashion Industry',0,0),(26,'Fertilizer Industry',0,0),(27,'Film Industry',0,0),(28,'Food Processing Industry',0,0),(29,'Furniture Industry',0,0),(30,'Garment Industry',0,0),(31,'Granite Industry',0,0),(32,'Health Care Industry',0,0),(33,'Hotel Industry',0,0),(34,'Insurance Industry',0,0),(35,'IT Industry',0,0),(36,'Jewellery Industry',0,0),(37,'Jute Industry',0,0),(38,'Leather Industry',0,0),(39,'Mining Industry',0,0),(40,'Music Industry',0,0),(41,'Mutual Fund Industry',0,0),(42,'Oil Industry',0,0),(43,'Paint Industry',0,0),(44,'Paper Industry',0,0),(45,'Pearl Industry',0,0),(46,'Pharmaceutical Industry',0,0),(47,'Plastic Industry',0,0),(48,'Poultry Industry',0,0),(49,'Power Industry',0,0),(50,'Printing Industry',0,0),(51,'Railway Industry',0,0),(52,'Real Estate Industry',0,0),(53,'Retail Industry',0,0),(54,'Rubber Industry',0,0),(55,'Shipping Industry',0,0),(56,'Silk Industry',0,0),(57,'Soap Industry',0,0),(58,'Solar Industry',0,0),(59,'Steel Industry',0,0),(60,'Sugar Industry',0,0),(61,'Tea Industry',0,0),(62,'Telecom Industry',0,0),(63,'Television Industry',0,0),(64,'Textile Industry',0,0),(65,'Tobacco Industry',0,0),(66,'Tourism Industry',0,0),(67,'Toy Industry',0,0),(68,'Tractor Industry',0,0),(69,'Turbine Industry',0,0),(70,'Weaving Industry',0,0),(71,'Zinc Industry',0,0);");
    }

    public function down()
    {
        echo "m151008_085746_insert_industry_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
