<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_105038_create_project_media_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `project_media` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `path` varchar(150) NOT NULL,
                          `project_id` int(11) unsigned NOT NULL,
                          `type` enum('image','video','document') DEFAULT NULL COMMENT 'Media type for Images, Videos and Document . ',
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `fk_project_media_project_id_idx` (`project_id`),
                          CONSTRAINT `fk_project_media_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
    }

    public function down()
    {
        echo "m150921_105038_create_project_media_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
