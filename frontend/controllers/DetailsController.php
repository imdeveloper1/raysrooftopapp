<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use frontend\models\Details;
use yii\db\Connection;
use yii\web\UploadedFile;

class DetailsController extends ActiveController {

    public $modelClass = 'frontend\models\Details';

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAddDetails() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (!empty($request_data->connection_type) && !empty($request_data->contract_load_value) && !empty($request_data->contract_load_unit) && !empty($request_data->roof_type) && !empty($request_data->roof_area)) {

            $detail = new Details;
            //Start code img upload     
//            if (isset($_FILES['roof_img1']) && !empty($_FILES['roof_img1'])) {
//                $uploadedFile = UploadedFile::getInstanceByName('roof_img1');
//                $file_name = '/upload_img/' . time() . "_" . str_replace(' ', '_', $uploadedFile->name);
//                $uploadPath = Yii::getAlias('@backend') . "/web" . $file_name;
//                $uploadedFile->saveAs($uploadPath);
//                $detail->roof_img1 = $file_name;
//            }
//
//            if (isset($_FILES['bill_img1']) && !empty($_FILES['bill_img1'])) {
//                $uploadedFile2 = UploadedFile::getInstanceByName('bill_img1');
//                $file_name2 = '/upload_img/' . time() . "_" . str_replace(' ', '_', $uploadedFile2->name);
//                $uploadPath2 = Yii::getAlias('@backend') . "/web" . $file_name2;
//                $uploadedFile2->saveAs($uploadPath2);
//                $detail->bill_img1 = $file_name2;
//            }     
            //End code img upload             
            $detail->connection_type = $request_data->connection_type;
            $detail->contract_load_value = $request_data->contract_load_value;
            $detail->contract_load_unit = $request_data->contract_load_unit;
            $detail->roof_type = $request_data->roof_type;
            $detail->roof_inclination = $request_data->roof_inclination;
            $detail->roof_area = $request_data->roof_area;
            $detail->roof_area_unit = $request_data->roof_area_unit;
            $detail->monthly_unit = $request_data->monthly_unit;
            $detail->monthly_bill = $request_data->monthly_bill;
            $detail->status = '1';
            $detail->created_at = time();
            $detail->updated_at = time();

            $first_year_gen = null;
            $twentyfive_year_gen = null;
            $units_generated_per_month = null;
            $saving_per_month = null;
            $system_size = $this->get_system_size($detail); // Function for get system size      
            //$system_size = 1;
            if ($system_size) {
                $first_year_gen = $this->get_first_year_generation($system_size); // Function for get 1st year generation
                $twentyfive_year_gen = $this->get_twentyfive_year_generation($first_year_gen); // Function for get 25th year generation
                $units_generated_per_month = $this->get_units_generated_per_month($system_size); // Function for get units genrated per month 
                $saving_per_month = $this->get_saving_per_month($units_generated_per_month, $request_data->connection_type); // Function for get saving per month 
                $saving_twentyfive_year = $this->get_saving_twentyfive_year($saving_per_month); // Function for get saving 25 years 
            }
            $detail->system_size = $system_size;
            $detail->first_year_generation = $first_year_gen;
            $detail->twentyfive_year_generation = $twentyfive_year_gen;
            $detail->saving_per_month = $saving_per_month;
            $detail->saving_twentyfive_year = $saving_twentyfive_year;

            if ($detail->save()) {
                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'Detail Added Succefully.';
                $response_array['responseData']['data'] = $detail;
                return $response_array;
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'The detail could not be saved!';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    public function actionUploadDetailImg() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (isset($request_data->detail_id) && !empty($request_data->detail_id)) {
            $detail = $this->findModel($request_data->detail_id);
            if (!empty($_FILES)) {
                $first_key = key($_FILES);
                $img_path = yii::$app->params['imagesPath'];
                if (!empty($first_key)) {
                    $uploadedFile = UploadedFile::getInstanceByName($first_key);
                    $file_name = '/upload_img/' . time() . "_" . str_replace(' ', '_', $uploadedFile->name);
                    $uploadPath = Yii::getAlias('@backend') . "/web" . $file_name;
                    $uploadedFile->saveAs($uploadPath);
                    $detail->$first_key = $img_path .'backend/web'. $file_name;
                } else {
                    $response_array['statusCode'] = 500;
                    $response_array['status'] = 'failure';
                    $response_array['responseData']['message'] = 'File not found!';
                    return $response_array;
                }
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'File has been not attached!';
                return $response_array;
            }
            //End code img upload                    
            if ($detail->save()) {
                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'Image uploaded successfully.';
                //$response_array['responseData']['data'] = '';
                return $response_array;
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'The Image could not be uploaded!';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    private function get_system_size($data) {
        $contract_load = 0;
        $roof_area_cal = 0;
        if (isset($data['contract_load_unit']) && !empty($data['contract_load_unit']) && isset($data['contract_load_value']) && !empty($data['contract_load_value'])) {
            if ($data['contract_load_unit'] == 'kVA') {
                $pf = 0.8; // Power factor
                $kw = $data['contract_load_value'] * $pf;   // kW = kVA*PF
            } else {
                $kw = $data['contract_load_value'];
            }
            $contract_load = ($kw * 80) / 100;  // 80% of contract load according to government rule 
        } if (!empty($data['roof_area']) && !empty($data['roof_area_unit'])) {
            switch ($data['roof_area_unit']) {
                case 'sq.ft.':
                    $roof_area = $data['roof_area'];
                    break;
                case 'sq.m.':
                    $roof_area = $data['roof_area'];
                    $roof_area = $roof_area * 10.764;  //ft² = m² * 10.764
                    break;
                case 'sq.yds.':
                    $roof_area = $data['roof_area'];
                    $roof_area = $roof_area * 9;   //ft² =yd² * 9.0000
                    break;
            }
            $roof_area_cal = $roof_area / 100;
        }
        if ($contract_load > $roof_area_cal) {
            return $roof_area_cal;
        } else {
            return $contract_load;
        }
    }

    private function get_first_year_generation($size = null) {
        return 125 * 12 * $size;   //1st year generation = 125*12*System Size
    }

    private function get_twentyfive_year_generation($first = null) {
        //return 25 * $first;   //25th years generation = 25*1st Year Generation                
        $total = 0;
        $total_per_year = 0;
        for ($i = 2; $i <= 25; $i++) {
            if ($i == 2) {
                $year_per = ($first * 1) / 100;             // 1% decrease in first year genration
                $total_per_year = $first - $year_per;
                $total = $total_per_year + $first;
            } else {
                $year_per = ($total_per_year * 1) / 100;     // 1% deduction in previous year genration
                $total_per_year = $total_per_year - $year_per;
                $total = $total + $total_per_year;
            }
        }
        return $total;
    }

    private function get_units_generated_per_month($size = null) {
        return 125 * $size;   //Units generated per month = 125*system size
    }

    private function get_saving_per_month($units_per_month = null, $connection_type = null) {
        //Savings per month = Units generated per month multiplied by connection type like: domestic,commercial,industrial,institutional
        $saving_per_month = 0;
        switch ($connection_type) {
            case 'domestic':
                $saving_per_month = $units_per_month * 6;  //6.00 for domestic
                break;
            case 'commercial':
                $saving_per_month = $units_per_month * 7;  //7.00 for commercial
                break;
            case 'industrial':
                $saving_per_month = $units_per_month * 7.50;   //7.50 for industrial
                break;
            case 'institutional':
                $saving_per_month = $units_per_month * 8.50;   //8.50 for institutional
                break;
        }
        return $saving_per_month;
    }

    private function get_saving_twentyfive_year($saving_per_month = null) {
        return 12 * 25 * $saving_per_month;   //saving twentyfive years = 12 * 25 * saving per month (months*years*saving_per_month)
    }

    protected function findModel($id) {
        if (($model = Details::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
