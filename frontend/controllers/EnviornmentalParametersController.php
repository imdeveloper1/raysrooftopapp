<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use frontend\models\EnviornmentalParameters;
use yii\db\Connection;

class EnviornmentalParametersController extends ActiveController
{
    public $modelClass = 'frontend\models\EnviornmentalParameters';
    public function actionIndex()
    {        
        return $this->render('index');
    }
    
    public function actionGetEnviornmentalparameters(){
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }
        $res = EnviornmentalParameters::find()->select('id, km, co_produced, fuel_saved, trees_planted, cars_removed')->one();        
        if ($res) {
            $response_array['statusCode'] = 200;
            $response_array['status'] = 'success';
            $response_array['responseData']['message'] = 'success';
            $response_array['responseData']['data'] = $res;
            return $response_array;
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Record not found!';
            return $response_array;
        }
    }

}
