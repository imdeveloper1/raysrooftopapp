<?php

namespace frontend\controllers;

use frontend\components\Controller;
use Yii;
use common\models\LoginForm;
use yii\web\NotFoundHttpException;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\GuestUsers;
use backend\models\Settings;
use backend\models\Application;
use frontend\models\Applicationuae;
use frontend\models\Applicationschengen;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'add-user'],
                'rules' => [
                    [
                        'actions' => ['logout','add-user'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $model = \yii\helpers\ArrayHelper::map(\common\models\Contents::find()->all(), 'page', 'content');
        return $this->render('index', ['model' => $model]);
    }
    
    public function actionLogin() {
        return $this->redirect(['index']);
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionStaffLogin() {
        $this->layout = 'main-inner';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->staffLogin()) {
            return true;
            //return $this->redirect(['add-user']);
        } else {
            return false;
            //return $this->render('staff-login', ['model' => $model,]);
        }
    }

    public function actionGuestLogin() {
        $this->layout = 'main-inner';
        $model = new GuestUsers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $session = Yii::$app->session;
            $session->set('guest_id', $model->id);
            return true;
            //return $this->redirect(['country-selection']);
        } else {
            return false;
            //return $this->render('guest-login', ['model' => $model]);
        }
    }

    public function actionAddUser() {
        $this->layout = 'main-inner';
        $model = new GuestUsers();

        if (!Yii::$app->user->isGuest)
            $model->created_by = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['country-selection', 'id' => $model->id]);
        } else {
            return $this->render('add-user', [
                        'model' => $model,
            ]);
        }
    }
    
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogoutStaff() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionLogoutGuest() {
        $session = Yii::$app->session;
        $session->remove('guest_id');
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = \yii\helpers\ArrayHelper::map(\common\models\Contents::find()->all(), 'page', 'content');
        $this->layout = 'main-inner';
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionNecessaryDocuments() {
        $model = \yii\helpers\ArrayHelper::map(\common\models\Contents::find()->all(), 'page', 'content');
        $this->layout = 'main-inner';
        return $this->render('necessary-documents', [
                    'model' => $model,
        ]);
    }

}
