<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use frontend\models\Plants;
use yii\db\Connection;

class PlantsController extends ActiveController {

    public $modelClass = 'frontend\models\Plants';

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionGetallplants() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;

        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (isset($request_data->zipcode) && !empty($request_data->zipcode)) {

            $lat_long = $this->getLnt($request_data->zipcode);
            if (!empty($lat_long)) {
                $sql = "SELECT *,round( (3959 * acos( cos( radians(" . $lat_long['lat'] . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $lat_long['lng'] . ") ) + sin( radians(" . $lat_long['lat'] . ") ) * sin( radians( latitude ) ) ) ),2) AS distance FROM `plants` having distance < 20"; // 20 for 20miles            
                $model = Plants::findBySql($sql)->asArray()->all();
                if (!empty($model)) {
                    $response_array['statusCode'] = 200;
                    $response_array['status'] = 'success';
                    $response_array['responseData']['message'] = 'Find Plants';
                    $response_array['responseData']['data_array'] = $model;
                    return $response_array;
                } else {
                    $response_array['statusCode'] = 500;
                    $response_array['status'] = 'failure';
                    $response_array['responseData']['message'] = 'Record not found!';
                    return $response_array;
                }
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'Please enter correct zipcode!';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    function getLnt($zip) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($zip) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        if ($result['status'] == 'OK') {
            $result1[] = $result['results'][0];
            $result2[] = $result1[0]['geometry'];
            $result3[] = $result2[0]['location'];
            return $result3[0];
        }
        return;
    }

}
