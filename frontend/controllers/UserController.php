<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use frontend\models\User;
use frontend\models\EmailTemplate;
use yii\db\Connection;

class UserController extends ActiveController {

    public $modelClass = 'frontend\models\User';

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionGetalluser() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        $res = User::find()->select('id, username, email, phone_no, address, status, type')->all();
        $response_array['statusCode'] = 200;
        $response_array['status'] = 'success';
        $response_array['responseData']['message'] = 'success';
        $response_array['responseData']['data_array'] = $res;
        return $response_array;
    }

    public function actionGetuserbyid() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;

        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (isset($request_data->id) && !empty($request_data->id)) {
            $user_exist = User::find()->select('id, username, email, phone_no, address, status, type')->where(array('id' => $request_data->id))->one();
            if (!empty($user_exist)) {
                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'Profile detail!';
                $response_array['responseData']['data'] = $user_exist;
                return $response_array;
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'User not exist!';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    public function actionChecklogin() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;

        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (isset($request_data->email) && !empty($request_data->password)) {

            $username_mail = User::find()->where(array('type' => 'user', 'email' => $request_data->email, 'status' => '1'))->count();

            if ($username_mail > 0) {

                $password_exist = User::find()->where(array('type' => 'user', 'email' => $request_data->email, 'password' => md5($request_data->password)))->count();

                if ($password_exist > 0) {
                    $res = User::find()->select('id, username, email, phone_no, address, status, type')->where(array('type' => 'user', 'email' => $request_data->email, 'password' => md5($request_data->password)))->asArray()->one();
                    $response_array['statusCode'] = 200;
                    $response_array['status'] = 'success';
                    $response_array['responseData']['message'] = 'Login Successful.';
                    $res['id'] = (int) $res['id'];
                    $response_array['responseData']['data'] = $res;
                    return $response_array;
                } else {
                    $response_array['statusCode'] = 500;
                    $response_array['status'] = 'failure';
                    $response_array['responseData']['message'] = 'Password do not match!';
                    return $response_array;
                }
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'Email/User does not exist';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    public function actionAdduser() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;

        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (!empty($request_data->email) && !empty($request_data->phone_no)) {
            $user = new User;
            $user->type = 'user';
            $user->username = $request_data->name;
            $user->email = $request_data->email;
            //$user->password = md5($request_data->password);
            $user->phone_no = $request_data->phone_no;
            $user->address = $request_data->address;

            if ($user->save()) {
                $email_template = EmailTemplate::find()->where(array('name' => 'User_Registration'))->one();
                $this->send_email($user->email,$email_template->subject,$email_template->description);
//                $message = Yii::$app->mailer->compose()
//                        ->setFrom('ravi.chouhan@girnarsoft.com')
//                        //->setTo('nikhil.gupta@raysexperts.com')
//                        ->setTo($user->email)
//                        ->setSubject($email_template->subject)
//                        ->setHtmlBody($email_template->description)
//                        ->send();
                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'User Added Succefully!';
                $response_array['responseData']['data'] = $user;
                return $response_array;
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';

                if (isset($user->getErrors()['email'][0]))
                    $response_array['responseData']['message'] = $user->getErrors()['email'][0];
                else
                    $response_array['responseData']['message'] = 'Failed';

                //$response_array['responseData']['data'] = $user->getErrors();
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    public function actionForgotPassword() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (isset($request_data->email)) {
            $user_exist = User::find()->where(array('email' => $request_data->email))->count();

            if ($user_exist > 0) {
                $new_password = $this->random_password();
                $posts = Yii::$app->db->createCommand()
                        ->update('user', ['password' => md5($new_password)], 'email = "' . $request_data->email . '"')
                        ->execute();
                
                $message = '<h2>Hi, your new password is : '.$new_password.'</h2>';
                $this->send_email($request_data->email, 'RAYS : Forgot Password', $message);
//                $message = Yii::$app->mailer->compose('@app/mail/forgotpassword', ['newpassword' => $new_password,])
//                        ->setFrom('anil@girnarsoft.com')
//                        ->setTo($request_data->email)
//                        ->setSubject('RAYS : Forgot Password')
//                        ->send();

                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'Email Sent to your given mail account.';
                return $response_array;
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'Email does not exist!';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    public function random_password($length = 8) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    public function send_email($to,$subject,$message) {
        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
//        $headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
        $headers .= 'From: Rays RoofTop <rays.experts26@gmail.com>' . "\r\n";
//        $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
//        $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

// Mail it
        mail($to, $subject, $message, $headers);
    }

}
