<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use frontend\models\Zipcodes;
use frontend\models\EmailTemplate;
use yii\db\Connection;

class ZipcodesController extends ActiveController {

    public $modelClass = 'frontend\models\Zipcodes';

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionCheckzipcode() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;

        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }
        if (isset($request_data->zipcode) && !empty($request_data->zipcode)) {
            $username_mail = Zipcodes::find()->where(array('zipcode' => $request_data->zipcode))->count();
            if ($username_mail > 0) {
                if (isset($request_data->email) && !empty($request_data->email)) {
                    $email_template = EmailTemplate::find()->where(array('name' => 'Zipcode_Success'))->one();
                    $this->send_email($request_data->email,$email_template->subject,$email_template->description);
//                    $message = Yii::$app->mailer->compose()
//                            ->setFrom('anil@girnarsoft.com')
//                            ->setTo($request_data->email)
//                            ->setSubject($email_template->subject)
//                            ->setHtmlBody($email_template->description)
//                            ->send();
                }
                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'Zipcode Found.';
                //$response_array['responseData']['data'] = '';
                return $response_array;
            } else {
                if (isset($request_data->email) && !empty($request_data->email)) {
                    $email_template = EmailTemplate::find()->where(array('name' => 'Zipcode_Cancel'))->one();
                    $this->send_email($request_data->email,$email_template->subject,$email_template->description);
//                    $message = Yii::$app->mailer->compose()
//                            ->setFrom('anil@girnarsoft.com')
//                            ->setTo($request_data->email)
//                            ->setSubject($email_template->subject)
//                            ->setHtmlBody($email_template->description)
//                            ->send();
                }
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'Zipcode Not Found';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }
    
    public function send_email($to,$subject,$message) {
        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
        $headers .= 'From: Rays RoofTop <rays.experts26@gmail.com>' . "\r\n";
// Mail it
        mail($to, $subject, $message, $headers);
    }

}
