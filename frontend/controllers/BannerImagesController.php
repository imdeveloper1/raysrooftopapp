<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use common\models\BannerImages;
use yii\db\Connection;

class BannerImagesController extends ActiveController {

    public $modelClass = 'common\models\BannerImages';

    public function actionIndex() {
        return $this->render('index');
    }
    
    public function actionGetsplashscreen() {
    $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        
//        $headers_data = '';
//        $access_params = '';
//        $access_permission = 0;
//        $headers_data = yii::$app->request->getHeaders();
//        $access_params = yii::$app->params;
//        if ($access_params['x_rest_username'] == $headers_data['x_rest_username']) {
//            if ($access_params['x_rest_password'] == $headers_data['x_rest_password']) {
//                $access_permission++;
//            }
//        }        
//        if ($access_permission == 0) {
//             $response_array['statusCode'] = 500;
//            $response_array['status'] = 'failure';
//            $response_array['responseData']['message'] = 'Unauthorized Access !!';
//            return $response_array;
//        } 
        
        
//        if (!isset($request_data)) {
//            $response_array['statusCode'] = 500;
//            $response_array['status'] = 'failure';
//            $response_array['responseData']['message'] = 'Please provide input data!';
//            return $response_array;
//        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
//            $response_array['statusCode'] = 500;
//            $response_array['status'] = 'failure';
//            $response_array['responseData']['message'] = 'Unauthorized Access !!';
//            return $response_array;
//        }
        
        $res = BannerImages::find()->select('id, title, image, logo, description, type')->where(array('status' => '1'))->all();
        if ($res) {
            $img_path = yii::$app->params['imagesPath'];
            foreach ($res as $data){
                $data->image = $img_path.'backend/web'.$data->image;
                $data->logo = $img_path.'backend/web'.$data->logo;
                $data->type = $data->type==2 ? 'splash_screen' : 'slider';
            }
            $response_array['statusCode'] = 200;
            $response_array['status'] = 'success';
            $response_array['responseData']['message'] = 'success';
            $response_array['responseData']['data_array'] = $res;
            return $response_array;
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Record not found!';
            return $response_array;
        }
    }

//    public function actionGetsplashscreen() {
//        $response_array = array();
//        $access_key = yii::$app->params['access_key'];
//        $request_data = (object) $_POST;
//        if (!isset($request_data)) {
//            $response_array['statusCode'] = 500;
//            $response_array['status'] = 'failure';
//            $response_array['responseData']['message'] = 'Please provide input data!';
//            return $response_array;
//        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
//            $response_array['statusCode'] = 500;
//            $response_array['status'] = 'failure';
//            $response_array['responseData']['message'] = 'Unauthorized Access !!';
//            return $response_array;
//        }
//
//        $res = BannerImages::find()->select('id, title, image, description')->where(array('status' => '1', 'type' => '2'))->one();
//        if ($res) {
//            $res->image = yii::$app->params['imagesPath'].$res->image;
//            $response_array['statusCode'] = 200;
//            $response_array['status'] = 'success';
//            $response_array['responseData']['message'] = 'success';
//            $response_array['responseData']['data_array'] = $res;
//            return $response_array;
//        } else {
//            $response_array['statusCode'] = 500;
//            $response_array['status'] = 'failure';
//            $response_array['responseData']['message'] = 'Record not found!';
//            return $response_array;
//        }
//    }

    public function actionGetslider() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        $res = BannerImages::find()->select('id, title, image, logo, description')->where(array('status' => '1', 'type' => '1'))->all();
        if ($res) {
            $img_path = yii::$app->params['imagesPath'];
            foreach ($res as $data){
                $data->image = $img_path.'backend/web'.$data->image;
                $data->logo = $img_path.'backend/web'.$data->logo;
            }
            $response_array['statusCode'] = 200;
            $response_array['status'] = 'success';
            $response_array['responseData']['message'] = 'success';
            $response_array['responseData']['data_array'] = $res;
            return $response_array;
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Record not found!';
            return $response_array;
        }
    }

}
