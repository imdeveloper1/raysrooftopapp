<?php

namespace frontend\controllers;

use yii;
use yii\rest\ActiveController;
use frontend\models\Notifications;
use frontend\models\EmailTemplate;
use yii\db\Connection;

class NotificationsController extends ActiveController {

    public $modelClass = 'frontend\models\Notifications';

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAddnotification() {
        $response_array = array();
        $access_key = yii::$app->params['access_key'];
        $request_data = (object) $_POST;
        if (!isset($request_data)) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide input data!';
            return $response_array;
        } else if (!isset($request_data->access_key) || $request_data->access_key != $access_key) {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Unauthorized Access !!';
            return $response_array;
        }

        if (!empty($request_data->zipcode) && !empty($request_data->email)) {
            $notification = new Notifications;
            $notification->created_at = time();
            $notification->updated_at = time();
            $notification->email = $request_data->email;
            $notification->zipcode = $request_data->zipcode;
            $notification->status = '1';

            if ($notification->save()) {
                $email_template = EmailTemplate::find()->where(array('name' => 'Zipcode_Save'))->one();
                $this->send_email($request_data->email,$email_template->subject,$email_template->description);
//                $message = Yii::$app->mailer->compose()
//                        ->setFrom('anil@girnarsoft.com')
//                        ->setTo($request_data->email)
//                        ->setSubject($email_template->subject)
//                        ->setHtmlBody($email_template->description)
//                        ->send();
                $response_array['statusCode'] = 200;
                $response_array['status'] = 'success';
                $response_array['responseData']['message'] = 'Notification Added Succefully';
                $response_array['responseData']['data'] = $notification;
                return $response_array;
            } else {
                $response_array['statusCode'] = 500;
                $response_array['status'] = 'failure';
                $response_array['responseData']['message'] = 'Notification could not be saved!';
                return $response_array;
            }
        } else {
            $response_array['statusCode'] = 500;
            $response_array['status'] = 'failure';
            $response_array['responseData']['message'] = 'Please provide valid input data!';
            return $response_array;
        }
    }

    public function send_email($to,$subject,$message) {
        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
        $headers .= 'From: Rays RoofTop <rays.experts26@gmail.com>' . "\r\n";
// Mail it
        mail($to, $subject, $message, $headers);
    }
}
