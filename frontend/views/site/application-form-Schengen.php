<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Application */
/* @var $form yii\widgets\ActiveForm */
$arr = array();

if (isset($result))
    $arr = $result;

$states = ArrayHelper::map(\common\models\UsState::getStates(), 'id', 'name');
?>

<div class="application-form" style="margin-top: 60px;">
    <h3>Apply For <?php echo (isset($pid) && $pid != '') ? $pid : '' ?></h3>

    <?php
    if (!isset($pid) || $pid == '') {
        $form2 = ActiveForm::begin();
        echo $form2->field($model, 'country')->dropDownList([ 'UAE' => 'UAE', 'UK' => 'UK', 'Schengen' => 'Schengen', 'Canada' => 'Canada', 'US' => 'US'], ['prompt' => '--Select Country--', 'onchange' => 'formsubmit(this.form,this.value)', 'name' => 'country', 'options' => [ '' => ['selected ' => true]]]);
        ActiveForm::end();
    }
    ?>
    <?php
    $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <?php
    if (isset($pid)) {
        if ($pid == 'UAE') {
            echo "GCC Residents who are Not GCC nationals but hold high professional status such as company managers, doctors, engineers or employees working in the public sector and their families are eligible for a Non-renewable 30-day visa upon arrival at all UAE airports or other points of entry. Original passport of the sponsored person, valid residency visa and proof of employment in the country of residence is required for visa. For other individuals the following are the requirement :";
        }
    }
    ?>


    <input type="hidden" name="cid" id="cid" value="<?php
    if (isset($pid)) {
        echo $pid;
    }
    ?>">

    <?php if (in_array('fullname', $arr)) { ?>
        <?= $form->field($model, 'fullname')->textInput(['maxlength' => true])->label('* Full Name'); ?>
    <?php } if (in_array('dob', $arr)) { ?>
        <?=
        $form->field($model, 'dob')->widget(DatePicker::ClassName(), [
            'name' => 'dob',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Date of Birth');
        ?>
    <?php } if (in_array('email', $arr)) { ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('* Email ID'); ?>
    <?php } if (in_array('phone_no', $arr)) { ?>
        <?php if ($pid == 'UAE') { ?>
            <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true, 'onchange' => 'totalamount()'])->label('* Contact No'); ?>
        <?php } else { ?>
            <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true])->label('* Contact No'); ?>
        <?php } ?>


    <?php } if (in_array('upload_passport', $arr)) { ?>
        <?php
        if (isset($pid)) {
            if ($pid == 'UAE') {
                ?>
                <?php //$form->field($model, 'upload_passport', ['labelOptions' => ['class' => null]])->fileInput()->label(false) ?>
                <?php echo $form->field($model, 'upload_passport')->fileInput()->label('* Passport'); ?>
                <?php if ($model->upload_passport) { ?>
                    <?php
                    $up = explode('.', $model->upload_passport);
                    if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
                        $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
                        $path = str_replace($basepath, '', $model->upload_passport);
                        echo Html::a('view/download', $path, ['labeldfsaf', 'target' => '_blank']);
                    } else {

                        echo '<img src="' . $model->upload_passport . '"  width="100" height="100">';
                    }
                    ?>


                <?php } ?>
            <?php }
        }
        ?>



    <?php } if (in_array('upload_picture', $arr)) { ?>
        <?php echo $form->field($model, 'upload_picture')->fileInput()->label('* Photo'); ?>
        <?php if ($model->upload_picture) { ?>
            <img src="<?= Yii::$app->request->baseUrl . $model->upload_picture ?>" class=" img-responsive" width='100' height='100' >
        <?php } ?>
    <?php } if (in_array('current_country_residence', $arr)) { ?>
        <?= $form->field($model, 'current_country_residence')->textInput(['maxlength' => true])->label('* Current Country of Residence'); ?>
    <?php } if (in_array('travel_from_date', $arr)) { ?>
        <?=
        $form->field($model, 'travel_from_date')->widget(DatePicker::ClassName(), [
            'name' => 'travel_from_date',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Date of Travel');
        ?>
    <?php } if (in_array('travel_to_date', $arr)) { ?>
        <?=
        $form->field($model, 'travel_to_date')->widget(DatePicker::ClassName(), [
            'name' => 'travel_to_date',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ]);
        ?>
    <?php } if (in_array('address_in_kuwait', $arr)) { ?>
        <?= $form->field($model, 'address_in_kuwait')->textInput(['maxlength' => true])->label('* Address'); ?>
    <?php } if (in_array('mother_name', $arr)) { ?>
        <?= $form->field($model, 'mother_name')->textInput(['maxlength' => true])->label('* Mother Full Name'); ?>
    <?php }if (in_array('mothers_nationality', $arr)) { ?>
        <?= $form->field($model, 'mothers_nationality')->textInput(['maxlength' => true])->label('* Mother Nationality'); ?>
    <?php } if (in_array('mother_dob', $arr)) { ?>
        <?=
        $form->field($model, 'mother_dob')->widget(DatePicker::ClassName(), [
            'name' => 'mother_dob',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Mother Date of Birth');
        ?>
    <?php } if (in_array('father_name', $arr)) { ?>
        <?= $form->field($model, 'father_name')->textInput(['maxlength' => true])->label('* Father Full Name '); ?>
    <?php } if (in_array('father_dob', $arr)) { ?>
        <?=
        $form->field($model, 'father_dob')->widget(DatePicker::ClassName(), [
            'name' => 'father_dob',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Father Date of Birth');
        ?>
    <?php } if (in_array('d_o_marriage', $arr)) { ?>
        <?=
        $form->field($model, 'd_o_marriage')->widget(DatePicker::ClassName(), [
            'name' => 'd_o_marriage',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('Date of Marriage (if married)');
        ?>

    <?php } if (in_array('marital_status', $arr)) { ?>
        <?= $form->field($model, 'marital_status')->dropDownList([ 'Single' => 'Single', 'Married' => 'Married', 'Divorced' => 'Divorced', 'Separated' => 'Separated', 'Others' => 'Others',], ['prompt' => '--Select--'])->label('* Marital Status'); ?>
    <?php } if (in_array('spouse_name', $arr)) { ?>
        <?= $form->field($model, 'spouse_name')->textInput(['maxlength' => true])->label('Spouse Name (if Married)'); ?>
    <?php } if (in_array('spouse_dob', $arr)) { ?>
        <?=
        $form->field($model, 'spouse_dob')->widget(DatePicker::ClassName(), [
            'name' => 'spouse_dob',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ]);
        ?>
    <?php } if (in_array('visa_validity_for', $arr)) { ?>
        <?php if ($pid == 'UAE') { ?>
            <?= $form->field($model, 'visa_validity_for')->dropDownList([ '30 Days' => '30 Days'])->label('* Visa Validity for'); ?>
        <?php } elseif ($pid == 'UK') { ?>
            <?= $form->field($model, 'visa_validity_for')->dropDownList([ '6 months' => '6 months', '2 Years' => '2 Years', '5 Years' => '5 Years', '10years' => '10years', 'Student visitor (up to 6 months)' => 'Student visitor (up to 6 months)', 'Student visitor - To study English Language' => 'Student visitor - To study English Language', 'Visitor in transit' => 'Visitor in transit', 'Student applicant and dependants (see student visa page for full definition)' => 'Student applicant and dependants (see student visa page for full definition)'])->label('* Visa Validity for'); ?>
        <?php } ?>
    <?php } if (in_array('occupation', $arr)) { ?>
        <?= $form->field($model, 'occupation')->dropDownList([ 'Student' => 'Student', 'Retired' => 'Retired', 'Employed' => 'Employed',], ['prompt' => '--Select --'])->label(' * Occupation'); ?>
    <?php } if (in_array('place_of_work', $arr)) { ?>
        <?= $form->field($model, 'place_of_work')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('job_title', $arr)) { ?>
        <?= $form->field($model, 'job_title')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('salary', $arr)) { ?>
        <?= $form->field($model, 'salary')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('name_of_school', $arr)) { ?>
        <?php if ($pid == 'Canada') { ?>
            <?= $form->field($model, 'name_of_school')->textInput(['maxlength' => true])->label('* Educational Details School/ University Name'); ?>
        <?php } else { ?>
            <?= $form->field($model, 'name_of_school')->textInput(['maxlength' => true])->label('Name of School/ College/ University (if Student)'); ?>   <?php } ?>
    <?php } if (in_array('field_of_study', $arr)) { ?>
        <?php if ($pid == 'Canada') { ?>
            <?= $form->field($model, 'field_of_study')->textInput(['maxlength' => true])->label('* Educational Details field of study'); ?>
        <?php } else { ?>
            <?= $form->field($model, 'field_of_study')->textInput(['maxlength' => true])->label('field_of_study'); ?>   <?php } ?>

    <?php } if (in_array('uk_visa_before', $arr)) { ?>
        <?= $form->field($model, 'uk_visa_before')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Were you granted a UK Visa Before?'); ?>
    <?php } if (in_array('before_type_of_visa', $arr)) { ?>
        <?= $form->field($model, 'before_type_of_visa')->textInput()->label('Type of Visa'); ?>

    <?php }if (in_array('before_place_of_issue', $arr)) { ?>
        <?= $form->field($model, 'before_place_of_issue')->textInput()->label('Place of Issue'); ?>

    <?php }if (in_array('before_date_of_travel', $arr)) { ?>
        <?=
        $form->field($model, 'before_date_of_travel')->widget(DatePicker::ClassName(), [
            'name' => 'before_date_of_travel',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select Date of Travel'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('Date of Travel');
        ?>
        <?php
    }
    if (in_array('before_date_of_return', $arr)) {
        ?>
        <?=
        $form->field($model, 'before_date_of_return')->widget(DatePicker::ClassName(), [
            'name' => 'before_date_of_return',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select Date of Return'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('Date of Return');
        ?>
    <?php }if (in_array('list_visited_countries', $arr)) { ?>
        <?= $form->field($model, 'list_visited_countries')->textInput()->label('List any countries visited in the last 10 years (list the dates and purpose of travel)'); ?>
    <?php }if (in_array('spend_on_living_cost', $arr)) { ?>
        <?= $form->field($model, 'spend_on_living_cost')->textInput(['maxlength' => true])->label('*How much do you spend on living cost'); ?>
    <?php }if (in_array('denied_visa_before', $arr)) { ?>
        <?= $form->field($model, 'denied_visa_before')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Have you been refused entry or denied a visa to the UK or any country in the last 10 years?'); ?>
    <?php } if (in_array('date_of_rejection', $arr)) { ?>
        <?= $form->field($model, 'date_of_rejection')->textInput()->label('Date of Rejection'); ?>

    <?php }if (in_array('rejection_ref_no', $arr)) { ?>
        <?= $form->field($model, 'rejection_ref_no')->textInput()->label('Reference Number'); ?>

    <?php }if (in_array('refused_country', $arr)) { ?>
        <?= $form->field($model, 'refused_country')->textInput()->label('Country for which you were refused'); ?>

        <?php
    }
    if (in_array('any_criminal_convictions', $arr)) {
        ?>
        <?= $form->field($model, 'any_criminal_convictions')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Any criminal convictions or terriorist activities involved in?'); ?>
    <?php } if (in_array('purpose_trip_us', $arr)) { ?>
        <?php if ($pid == 'Canada') { ?>
            <?= $form->field($model, 'purpose_trip_us')->textInput(['maxlength' => true])->label('* Purpose of Travel'); ?>
        <?php } else { ?>
            <?= $form->field($model, 'purpose_trip_us')->textInput(['maxlength' => true])->label('* Purpose of Trip to the U.S'); ?>
        <?php } ?>
    <?php } if (in_array('paying_trip', $arr)) { ?>
        <?= $form->field($model, 'paying_trip')->textInput(['maxlength' => true])->label('* Person/Entity Paying for Your Trip'); ?>
    <?php } if (in_array('other_traveling_with_you', $arr)) { ?>
        <?= $form->field($model, 'other_traveling_with_you')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Are there other persons traveling with you'); ?>
    <?php } if (in_array('which_state_visiting', $arr)) { ?>
        <?= $form->field($model, 'which_state_visiting')->dropDownList($states, ['prompt' => '--Select --'])->label('* Which state would you be visiting'); ?>
    <?php } if (in_array('have_visa_issue_us', $arr)) { ?>
        <?= $form->field($model, 'have_visa_issue_us')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Have you ever been issued a U.S. Visa?'); ?>
    <?php } if (in_array('have_refused_Visa', $arr)) { ?>
        <?= $form->field($model, 'have_refused_Visa')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Have you ever been refused a U.S. Visa, or been refused admission to the United States, or withdrawn your application for admission at the port of entry? '); ?>
    <?php } if (in_array('hold_nationality', $arr)) { ?>
        <?= $form->field($model, 'hold_nationality')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --']) ?>
    <?php } if (in_array('relatives_in_us', $arr)) { ?>
        <?= $form->field($model, 'relatives_in_us')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --']) ?>
    <?php } if (in_array('traveled_any_country', $arr)) { ?>
        <?= $form->field($model, 'traveled_any_country')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Have you traveled to any countries within the last five years?'); ?>
    <?php } if (in_array('member_state_destination', $arr)) { ?>
        <?= $form->field($model, 'member_state_destination')->textInput(['maxlength' => true])->label('* Member state(s) of destination'); ?>
    <?php } if (in_array('member_state_entry', $arr)) { ?>
        <?= $form->field($model, 'member_state_entry')->textInput(['maxlength' => true])->label('* Member state of first entry'); ?>
    <?php } if (in_array('number_entries_requeste', $arr)) { ?>
        <?= $form->field($model, 'number_entries_requeste')->textInput()->label('* Number of entries requested'); ?>
    <?php } if (in_array('days_intended_stay', $arr)) { ?>
        <?php
        if (isset($pid)) {
            if ($pid == 'Schengen') {
                ?>
                <?= $form->field($model, 'days_intended_stay')->textInput()->label('* Duration of intended stay (Indicate the number of days)'); ?>
            <?php } else { ?>
                <?= $form->field($model, 'days_intended_stay')->textInput()->label('* Intended Length of Stay in U.S'); ?>
                <?php
            }
        }
        ?>

    <?php } if (in_array('Schengen_visa_issued_photo', $arr)) { ?>
        <?php echo $form->field($model, 'Schengen_visa_issued_photo')->fileInput() ?>
        <?php if ($model->Schengen_visa_issued_photo) { ?>
            <?php
            $up = explode('.', $model->Schengen_visa_issued_photo);
            if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
                $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
                $path = str_replace($basepath, '', $model->Schengen_visa_issued_photo);
                echo Html::a('view/download', $path, ['labeldfsaf', 'target' => '_blank']);
            } else {

                echo '<img src="' . $model->Schengen_visa_issued_photo . '"  width="100" height="100">';
            }
            ?> <?php } ?>
    <?php } if (in_array('cost_of_travel', $arr)) { ?>
        <?= $form->field($model, 'cost_of_travel')->dropDownList([ 'Personally' => 'Personally', 'Sponsored' => 'Sponsored',], ['prompt' => '--Select --'])->label('* Cost of travel '); ?>
    <?php } if (in_array('means_of_support', $arr)) { ?>
        <?= $form->field($model, 'means_of_support')->dropDownList([ 'Cash' => 'Cash', 'Travellers check' => 'Travellers check', 'credit card' => 'credit card', 'pre-paid accommodations' => 'pre-paid accommodations', 'pre-paid transport' => 'pre-paid transport',], ['prompt' => '--Select --'])->label('* Means of support'); ?>
    <?php } if (in_array('travel_Ticket_image', $arr)) { ?>
        <?php echo $form->field($model, 'travel_Ticket_image')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Travel Ticket Copy'); ?>



    <?php } if (in_array('hotel_reservation', $arr)) { ?>
        <?php echo $form->field($model, 'hotel_reservation')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Hotel Reservation'); ?>

    <?php } if (in_array('current_employment_city', $arr)) { ?>
        <?= $form->field($model, 'current_employment_city')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('current_employment', $arr)) { ?>
        <?= $form->field($model, 'current_employment')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('current_employment_from', $arr)) { ?>

        <?=
        $form->field($model, 'current_employment_from')->widget(DatePicker::ClassName(), [
            'name' => 'current_employment_from',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Current Employment From');
        ?>



    <?php } if (in_array('current_employment_to', $arr)) { ?>
        <?=
        $form->field($model, 'current_employment_to')->widget(DatePicker::ClassName(), [
            'name' => 'current_employment_to',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Current Employment To');
        ?>

    <?php } if (in_array('current_occupation', $arr)) { ?>
        <?= $form->field($model, 'current_occupation')->textInput(['maxlength' => true])->label('* Current Occupation'); ?>
    <?php } if (in_array('current_occ_company_name', $arr)) { ?>
        <?= $form->field($model, 'current_occ_company_name')->textInput(['maxlength' => true])->label('* Current Occupation Company Name'); ?>
    <?php } if (in_array('current_occ_company_city', $arr)) { ?>
        <?= $form->field($model, 'current_occ_company_city')->textInput(['maxlength' => true])->label('* Current Occupation Company City'); ?>
    <?php } if (in_array('previous_employment', $arr)) { ?>
        <?= $form->field($model, 'previous_employment')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('previous_employment_from', $arr)) { ?>
        <?=
        $form->field($model, 'previous_employment_from')->widget(DatePicker::ClassName(), [
            'name' => 'previous_employment_from',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('Previous Employment From');
        ?>

    <?php } if (in_array('previous_employment_to', $arr)) { ?>
        <?=
        $form->field($model, 'previous_employment_to')->widget(DatePicker::ClassName(), [
            'name' => 'previous_employment_to',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('Previous Employment to');
        ?>

    <?php } if (in_array('previous_occupation', $arr)) { ?>
        <?= $form->field($model, 'previous_occupation')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('previous_occ_company_name', $arr)) { ?>
        <?= $form->field($model, 'previous_occ_company_name')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('previous_occ_company_city', $arr)) { ?>
        <?= $form->field($model, 'previous_occ_company_city')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('health_concerns', $arr)) { ?>
        <?= $form->field($model, 'health_concerns')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Do you have health concerns specially Tuberculosis'); ?>
    <?php } if (in_array('have_served_military', $arr)) { ?>
        <?= $form->field($model, 'have_served_military')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Have you ever served in the military?'); ?>
    <?php } if (in_array('civil_id_photo', $arr)) { ?>
        <?php echo $form->field($model, 'civil_id_photo')->fileInput()->label('* Civil Id'); ?>
        <?php if ($model->civil_id_photo) { ?>
            <?php
            $up = explode('.', $model->civil_id_photo);
            if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
                $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
                $path = str_replace($basepath, '', $model->civil_id_photo);
                echo Html::a('view/download', $path, ['target' => '_blank']);
            } else {

                echo '<img src="' . $model->civil_id_photo . '"  width="100" height="100">';
            }
            ?>      <?php } ?>

    <?php } if (in_array('kuwait_residence_photo', $arr)) { ?>
        <?php echo $form->field($model, 'kuwait_residence_photo')->fileInput()->label('* Kuwait Residence'); ?>
        <?php if ($model->kuwait_residence_photo) { ?>

            <?php
            $up = explode('.', $model->kuwait_residence_photo);
            if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
                $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
                $path = str_replace($basepath, '', $model->kuwait_residence_photo);
                echo Html::a('view/download', $path, ['target' => '_blank']);
            } else {

                echo '<img src="' . $model->kuwait_residence_photo . '"  width="100" height="100">';
            }
            ?>    <?php } ?>
    <?php } if (in_array('intended_date_travel', $arr)) { ?>
        <?= $form->field($model, 'intended_date_travel')->textInput(['maxlength' => true])->label('* Intended Date of Travel'); ?>
    <?php } if (in_array('period_of_stay', $arr)) { ?>
        <?= $form->field($model, 'period_of_stay')->textInput(['maxlength' => true])->label('* Period of Stay'); ?>
    <?php } if (in_array('hotel_name', $arr)) { ?>
        <?= $form->field($model, 'hotel_name')->radioList([ 'Yes' => 'Yes', 'No' => 'No',], ['prompt' => '--Select --'])->label('* Hotel Reservation'); ?>
    <?php } if (in_array('hotel_address', $arr)) { ?>
        <?= $form->field($model, 'hotel_address')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('educational_details', $arr)) { ?>
        <?= $form->field($model, 'educational_details')->textInput(['maxlength' => true]) ?>
    <?php } if (in_array('educational_from', $arr)) { ?>
        <?=
        $form->field($model, 'educational_from')->widget(DatePicker::ClassName(), [
            'name' => 'educational_from',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Educational Details from');
        ?>

    <?php } if (in_array('educational_to', $arr)) { ?>
        <?=
        $form->field($model, 'educational_to')->widget(DatePicker::ClassName(), [
            'name' => 'educational_to',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ])->label('* Educational Details to');
        ?>
    <?php } if (in_array('school_of_city', $arr)) { ?>
        <?= $form->field($model, 'school_of_city')->textInput(['maxlength' => true])->label('* Educational Details city'); ?>
    <?php } if (in_array('purpose_of_travel', $arr)) { ?>
        <?= $form->field($model, 'purpose_of_travel')->textInput(['maxlength' => true])->label('Purpose of Travel'); ?>
        <?php
    }if (in_array('cas_statement', $arr)) {
        echo $form->field($model, 'cas_statement')->fileInput()->label('If applying for a Student Tier 4 Visa please upload CAS statement');
        if ($model->cas_statement) {
            ?>
            <?php
            $up = explode('.', $model->cas_statement);

            if (end($up) == 'pdf') {

                $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
                $path = str_replace($basepath, '', $model->cas_statement);
                echo Html::a('view/download', $path, ['labeldfsaf', 'target' => '_blank']);
            } else {

                echo '<img src="' . $model->cas_statement . '"  width="100" height="100">';
            }
            ?> <?php
        }
    }
    ?>
    <?php
    if(isset($pid))
        echo $form->field($model, 'is_accepted_tc')->checkbox(['label' => '* Accept Terms and Conditions', 'required' => 'required']); 
    ?>
    <?php if (isset($pid)) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">

    function formsubmit(formsub, cid) {
        //alert(cid);
        formsub.submit();
        //   alert(document.getElementById('cid').value);
    }
    function totalamount()
    {
        submitFlag = true;
        var x = document.getElementById('applicationuae-phone_No').value;
        if (x.length != 8) {
            submitFlag = false;
            alert("invalid length - 8 needed!");
        }
        return submitFlag;

    }
</script>