<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>

<div class="application-form" style="margin-top: 60px;">
    <h3>Apply For <?php echo $pid; ?></h3>

    <?php $form = ActiveForm::begin(); ?>
    <input type="hidden" name="cid" id="cid" value="<?php if (isset($pid)) echo $pid; ?>">
    
    <h4>*Disclaimer : <h5>Form filling is not done at our Website</h5></h4>
    <?= $form->field($model, 'is_accepted_tc')->checkbox(['label' => '* Accept Terms and Conditions', 'required' => 'required']); ?>

    <div class="form-group">
        <?= Html::submitButton('Fill Form', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>