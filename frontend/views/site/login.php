<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

    <!-- Login Start Here -->
    <div id="form">
        <div class="formmain" id="login">
            <div class="formhead">CSR Rajasthan <span class="subformhead">Login Form</span></div>
            <?php $form = ActiveForm::begin([
                'id' => 'material-form-2',
                'options' => ['class' => 'material-form-2'],
            ]);
            ?>
            <div class="formouter">
                <ul class="clearfix">
                    <li>
                        <?= $form->field($model, 'username', ['labelOptions' => ['class' => '']]); ?>
                    </li>
                    <li>
                        <?= $form->field($model, 'password', ['labelOptions' => ['class' => '']])->passwordInput(); ?>
                    </li>
                    <li></li>
                    <li></li>
                    <li>
                        <div class="indisplay chkhold">
                            <div class="label_check labelcheck fromsprite">
                                <?= $form->field($model, 'rememberMe')->checkbox(); ?>
<!--                                <input name="remember_me" id="checkbox-1" value="1" type="checkbox">-->
<!--                                <label for="checkbox-1">Remember Me</label>-->
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>If you forgot your password you can </br><a href="TBD" title="reset it">reset it</a></p>
                    </li>

                </ul>
            </div>


            <div class="formouter">
                <div class="buttonblock">
                    <button title="submit">submit</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <!-- Login End Here -->




<?php /*
<div id="form">
    <div class="formmain" id="login">
        <div class="formhead">CSR Rajasthan <span class="subformhead">Login Form</span></div>
        <?php $form = ActiveForm::begin([
            'id' => 'material-form-2',
            'options' => ['class' => 'material-form-2'],
        ]);
        ?>
        <div class="formouter">
            <ul class="clearfix">
                <li>
                    <?= $form->field($model, 'username') ?>
                </li>

                <li><?= $form->field($model, 'password')->passwordInput() ?></li>

                <?php /*                <li><?= $form->field($model, 'rememberMe')->checkbox() ?></li> ; ?>

                <li>
                    <div class="indisplay chkhold has-js">
                        <div class="label_check labelcheck fromsprite">
                            <input name="remember_me" id="checkbox-1" value="1" type="checkbox">
                            <label for="checkbox-1" class="labelfocus">Remember Me</label>
                        </div>
                    </div>
                </li>


                <li>
                    <p>If you forgot your password you can
                        <br> <?= Html::a('reset it', ['site/request-password-reset']) ?></p>
                </li>
            </ul>
        </div>

        <div class="formouter">
            <div class="buttonblock">
                <?= Html::submitButton('Login', ['class' => 'buttonblock', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
*/
?>