<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>
 
<div class="application-form" style="margin-top: 60px;">
    <h3>Application Form <?php echo $model->country . '/' . $model->id ?> Step 2</h3>

    <?php $form = ActiveForm::begin(); ?>

    <div class="application-form" id="receipt">
        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <tr>
                    <th>Applicant Name</th>
                    <td><?php echo $model->fullname; ?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?php echo $model->email; ?></td>
                </tr>
                <tr>
                    <th>Visa Fees</th>
                    <td><?php echo $model->visa_fees; ?> KWD</td>
                </tr>
                <tr>
                    <th>Service Charge</th>
                    <td><?php echo $model->service_charge; ?> KWD</td>
                </tr>
                <tr>
                    <th>Value Added Services</th>
                    <td>
                        <table class="table table-striped table-bordered detail-view">
                            <tr>
                                <th></th>
                                <th>Service Name</th>
                                <th>Price</th>
                            </tr>
                            <?php
                            $services = common\models\Service::find()->where(array('country'=>$model->country))->all();
                                
                            foreach ($services as $service) {
                                echo "<tr>
                                        <td><input type='checkbox' name='services[]' value='$service->id'/></td>
                                        <td>$service->title</td>
                                        <td>$service->price KWD</td>
                                      </tr>";
                            }
                            ?>
                            <tr>
                                <td></td>
                                <td><b>VAS Total</b></td>
                                <td><b><input type='hidden' name='total_vas' class='total_vas'/><span class='total_vas'>0</span> KWD</b></td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>Total Amount</th>
                    <td><span id='total_amount'><?php echo $model->total_amount; ?></span> KWD</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Make Payment', ['class' => 'btn btn-success','onclick'=>'return validateForm()']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>