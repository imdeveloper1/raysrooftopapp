<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>

<div class="application-form" style="margin-top: 60px;">
    <h3>Country Selection</h3>

    <?php
    $form2 = ActiveForm::begin();
    echo $form2->field($model, 'country')->dropDownList(['Schengen' => 'Schengen', 'Canada' => 'Canada', 'US' => 'USA', 'UK' => 'UK'], ['prompt' => '--Select Country--', 'onchange' => 'formsubmit(this.form,this.value)', 'name' => 'country', 'options' => [ '' => ['selected ' => true]]]);
    ActiveForm::end();
    ?>
    
    <p style="width: 20%;float: left;">
        <?= Html::a('Apply For UAE', '/site/country-selection?country=UAE', ['class' => 'btn btn-success']) ?>
    </p>

</div>

<script type="text/javascript">

    function formsubmit(formsub, cid) {
        formsub.submit();
    }

</script>