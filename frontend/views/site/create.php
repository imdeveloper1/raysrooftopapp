<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GuestUsers */

$this->title = 'Create Guest User';
$this->params['breadcrumbs'][] = ['label' => 'Guest Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-users-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
