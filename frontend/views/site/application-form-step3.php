<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>

<div class="application-form" style="margin-top: 60px;">
    <h3>Application Form <?php echo $model->country . '/' . $model->id ?> Step 2</h3>

    <div class="application-form" id="receipt">
        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <tr>
                    <th>Applicant Name</th>
                    <td><?php echo $model->fullname; ?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?php echo $model->email; ?></td>
                </tr>
                <tr>
                    <th>Visa Fees</th>
                    <td><?php echo $model->visa_fees; ?> KWD</td>
                </tr>
                <tr>
                    <th>Service Charge</th>
                    <td><?php echo $model->service_charge; ?> KWD</td>
                </tr>
                <tr>
                    <th>Value Added Services</th>
                    <td>
                        <table class="table table-striped table-bordered detail-view">
                            <tr>
                                <th>Service Name</th>
                                <th>Price</th>
                            </tr>
                            <?php
                            $services = common\models\ApplicationServices::find()->where(array('application_id' => $model->id))->all();

                            foreach ($services as $service) {
                                echo "<tr>
                                        <td>$service->service_id</td>
                                        <td>$service->price KWD</td>
                                      </tr>";
                            }
                            ?>
                            <tr>
                                <td><b>VAS Total</b></td>
                                <td><b><?php echo $model->vas_fee; ?> KWD</b></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <th>Total Amount</th>
                    <td><?php echo $model->total_amount; ?> KWD</td>
                </tr>
            </tbody>
        </table>
    </div>

    <p style="width: 20%;float: left;">
        <?= Html::a('Print', '#', ['class' => 'btn btn-success', 'onclick' => 'printReceipt()']) ?>
    </p>
    <p style="width: 20%;float: left;">
        <?php
        $name = 'My Visa Application ' . $model->country . '/' . $model->id;
        $name = urlencode($name);
        echo Html::a('Export Application', "/site/application-pdf-view?name=$name", ['class' => 'btn btn-success'])
        ?>
    </p>
    <p style="width: 60%;float: left;">
        <?= Html::textInput('email', '', ['id' => 'email']); ?>
        <?= Html::a('Email', '#', ['class' => 'btn btn-success', 'onclick' => 'emailReceipt()']); ?>
    <h4 id='msg' style="color:blue;"></h4>
</p>

</div>
<script type="text/javascript">
    function printReceipt() {
        var data = $('#receipt').html();
        var mywindow = window.open('', "Receipt<?php echo $model->country . "/" . $model->id; ?>", 'height=400,width=600');
        mywindow.document.write("<html><head><title>Receipt<?php echo $model->country . "/" . $model->id; ?></title>");
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }

    function emailReceipt() {
        var email = $('#email').val();
        if (!email) {
            alert("Please Enter Vaild Email");
            return false;
        }
        var data = $('#receipt').html();
        var application_id = <?php echo $model->id; ?>;

        $.ajax({
            url: "/site/email-receipt",
            data: {"email": email, "data": data, "application_id": application_id},
            success: function(result) {
                $('#msg').text('Receipt Successfully Emailed to ' + email);
            }
        });

        return true;
    }
</script>