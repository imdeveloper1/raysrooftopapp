<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="form">
    <div class="formmain">
        <div class="formhead"><?= Html::encode($this->title) ?></div>


        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'options' => ['class' => 'material-form-2']]); ?>
        <div class="formouter">
            <p>Please fill out your email. A link to reset password will be sent there.</p>
            <ul class="clearfix">
                <li>
                    <?= $form->field($model, 'email') ?>
                </li>
            </ul>
            <div class="formouter">
                <div class="buttonblock">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>