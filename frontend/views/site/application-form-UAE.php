<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>

<div class="application-form" style="margin-top: 60px;">
    <h3>Apply For <?php echo (isset($pid) && $pid != '') ? $pid : '' ?></h3>

    <?php
    if (!isset($pid) || $pid == '') {
        $form2 = ActiveForm::begin();
        echo $form2->field($model, 'country')->dropDownList([ 'UAE' => 'UAE', 'UK' => 'UK', 'Schengen' => 'Schengen', 'Canada' => 'Canada', 'US' => 'US'], ['prompt' => '--Select Country--', 'onchange' => 'formsubmit(this.form,this.value)', 'name' => 'country', 'options' => [ '' => ['selected ' => true]]]);
        ActiveForm::end();
    }
    ?>
    <?php
    $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <?php
    if (isset($pid)) {
        if ($pid == 'UAE') {
            echo "GCC Residents who are Not GCC nationals but hold high professional status such as company managers, doctors, engineers or employees working in the public sector and their families are eligible for a Non-renewable 30-day visa upon arrival at all UAE airports or other points of entry. Original passport of the sponsored person, valid residency visa and proof of employment in the country of residence is required for visa. For other individuals the following are the requirement :";
        }
    }
    ?>


    <input type="hidden" name="cid" id="cid" value="<?php
    if (isset($pid)) {
        echo $pid;
    }
    ?>">


    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true])->label('* Full Name'); ?>
    <?= $form->field($model, 'address_in_kuwait')->textInput(['maxlength' => true])->label('* Address'); ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('* Email ID'); ?>

    <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true, 'onchange' => 'totalamount()'])->label('* Contact No'); ?>




    <?php
    if (isset($pid)) {
        if ($pid == 'UAE') {
            ?>
            <?php //$form->field($model, 'upload_passport', ['labelOptions' => ['class' => null]])->fileInput()->label(false) ?>
            <?php echo $form->field($model, 'upload_passport')->fileInput()->label('* Passport'); ?>
            <?php if ($model->upload_passport) { ?>
                <?php
                $up = explode('.', $model->upload_passport);
                if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
                    $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
                    $path = str_replace($basepath, '', $model->upload_passport);
                    echo Html::a('view/download', $path, ['labeldfsaf', 'target' => '_blank']);
                } else {

                    echo '<img src="' . $model->upload_passport . '"  width="100" height="100">';
                }
                ?>


            <?php } ?>
            <?php
        }
    }
    ?>




    <?php echo $form->field($model, 'upload_picture')->fileInput()->label('* Photo'); ?>
    <?php if ($model->upload_picture) { ?>
        <img src="<?= Yii::$app->request->baseUrl . $model->upload_picture ?>" class=" img-responsive" width='100' height='100' >
    <?php } ?>


    <?php echo $form->field($model, 'civil_id_photo')->fileInput()->label('* Civil Id'); ?>
    <?php if ($model->civil_id_photo) { ?>
        <?php
        $up = explode('.', $model->civil_id_photo);
        if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
            $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
            $path = str_replace($basepath, '', $model->civil_id_photo);
            echo Html::a('view/download', $path, ['target' => '_blank']);
        } else {

            echo '<img src="' . $model->civil_id_photo . '"  width="100" height="100">';
        }
        ?>      <?php } ?>


    <?php echo $form->field($model, 'kuwait_residence_photo')->fileInput()->label('* Kuwait Residence'); ?>
    <?php if ($model->kuwait_residence_photo) { ?>

        <?php
        $up = explode('.', $model->kuwait_residence_photo);
        if (end($up) == 'doc' || end($up) == 'pdf' || end($up) == 'docx') {
            $basepath = str_replace('\\', '/', Yii::$app->basePath) . '/web/';
            $path = str_replace($basepath, '', $model->kuwait_residence_photo);
            echo Html::a('view/download', $path, ['target' => '_blank']);
        } else {

            echo '<img src="' . $model->kuwait_residence_photo . '"  width="100" height="100">';
        }
        ?>    <?php } ?>

    <?php
    if (isset($pid))
        echo $form->field($model, 'is_accepted_tc')->checkbox(['label' => '* Accept Terms and Conditions', 'required' => 'required']);
    ?>
    <?php if (isset($pid)) { ?>
        <div class="form-group">
            <?= Html::submitButton('Apply', ['class' => 'btn btn-success']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">

    function formsubmit(formsub, cid) {
        //alert(cid);
        formsub.submit();
        //   alert(document.getElementById('cid').value);
    }
    function totalamount()
    {
        submitFlag = true;
        var x = document.getElementById('applicationuae-phone_No').value;
        if (x.length != 8) {
            submitFlag = false;
            alert("invalid length - 8 needed!");
        }
        return submitFlag;

    }
</script>