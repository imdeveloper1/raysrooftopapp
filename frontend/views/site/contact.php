<!-- Page Content -->
<main class="visa-contact-content">

    <!-- START Contact  section -->
    <div class="contact-parrallax">
        <h1>Contact US</h1>
        <div class="container section">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 text-center contact-address" >
                    <span class="icon-location icons"></span>
                    <h6>ADDRESS</h6> 
                    <p> <?php echo $model['contact_us_address']; ?></p>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-6 text-center contact-address" >
                    <span class="icon-phone-contact icons"></span>
                    <h6>Contact Us at</h6> 
                    <p> <?php echo $model['contact_us_at']; ?></p>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 text-center contact-address" >
                    <span class="icon-contact-add icons"></span>
                    <h6>EMAIL US AT</h6> 
                    <p> <?php echo $model['contact_us_email']; ?></p>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 text-center contact-address" >
                    <span class="icon-opening-hour icons"></span>
                    <h6>OFFICE OPENING HOURS</h6> 
                    <p> <?php echo $model['contact_us_office_opening_hours']; ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- /end Contact section -->

</main>