<!-- Page Content -->
<main class="visa-contact-content">

    <!-- How it works -->
    <div id="nesessarydoc">
        <div class="section">
            <div class="row text-center">
                <div class="nesessary-doc-heading">
                    <h1>Necessary Documents</h1>
                    <p>These are recommended documents that can be submitted with a visa application. For complete information on documentation please refer to the embassy link or follow the guideline on the visa application center website.</p>
                </div>
            </div>
            <div class="row">
                <div class="board">
                    <div class="board-inner container">
                        <ul class="nav nav-tabs col-md-12 liner" id="myTab" role="tablist">
                            <li class="active">
                                <a href="#countryOne" aria-controls="countryOne" role="tab" data-toggle="tab" >
                                    <span class="round-tabs one">
                                        <i class="flag-01 flags"></i>
                                    </span>
                                </a>
                                <strong>UK</strong>
                            </li>

                            <li>
                                <a href="#countryTwo" aria-controls="countryTwo" role="tab" data-toggle="tab">
                                    <span class="round-tabs two">
                                        <i class="flag-02 flags"></i>
                                    </span>
                                </a>
                                <strong>USA</strong>
                            </li>
                            <li>
                                <a href="#countryThird" aria-controls="countryThird" role="tab" data-toggle="tab" >
                                    <span class="round-tabs three">
                                        <i class="flag-03 flags"></i>
                                    </span>
                                </a>
                                <strong>Canada</strong>
                            </li>

                            <li>
                                <a href="#countryFour" aria-controls="countryFour" role="tab" data-toggle="tab" >
                                    <span class="round-tabs four">
                                        <i class="flag-04 flags"></i>
                                    </span>
                                </a>
                                <strong>UAE</strong> 
                            </li>

                            <li>
                                <a href="#countryFifth" aria-controls="countryFifth" role="tab" data-toggle="tab" >
                                    <span class="round-tabs five">
                                        <i class="flag-05 flags"></i>
                                    </span>
                                </a>
                                <strong>Schengen</strong> 
                            </li>

                        </ul>
                    </div>

                    <div class="tab-content row">
                        <div class="tab-pane fade in active" id="countryOne">
                            <div class="row necessary-doc-content text-left">
                                <div class="country-img">
                                    
                                    <img src="/img/country-01.jpg" alt="UK-Eidenburgh">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 necessary-content-right">
                                    <h3 class="head text-left"><span>UK</span></h3>
                                    <?php echo $model['necessary_documents_UK']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="countryTwo">
                            <div class="row necessary-doc-content text-left">
                                <div class="country-img">
                                    <img src="/img/country-02.jpg" alt="USA-Newyork">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 necessary-content-right">
                                    <h3 class="head text-left"><span>USA</span></h3>
                                    <?php echo $model['necessary_documents_US']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="countryThird">
                            <div class="row necessary-doc-content text-left">
                                <div class="country-img">
                                    <img src="/img/country-03.jpg" alt="CANADA-Toronto">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 necessary-content-right">
                                    <h3 class="head text-left"><span>Canada</span></h3>
                                    <?php echo $model['necessary_documents_Canada']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="countryFour">
                            <div class="row necessary-doc-content text-left">
                                <div class="country-img">
                                    <img src="/img/country-04.jpg" alt="UAE-Abu Dhabi">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 necessary-content-right">
                                    <h3 class="head text-left"><span>UAE</span></h3>
                                    <?php echo $model['necessary_documents_UAE']; ?>
                                </div>
                            </div>             
                        </div>
                        <div class="tab-pane fade" id="countryFifth">
                            <div class="row necessary-doc-content text-left">

                                <div class="country-img">
                                    <img src="/img/country-05.jpg" alt="Schengen-Swiss">
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 necessary-content-right">
                                    <h3 class="head text-left"><span>Schengen</span></h3>
                                    <?php echo $model['necessary_documents_Schengen']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- /How it works -->

</main>