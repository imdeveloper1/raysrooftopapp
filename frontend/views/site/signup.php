<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
?>
<div class="site-signup">




            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'email_repeat') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => 10]); ?>

            <?= $form->field($model, 'address')->label('Address')->textArea(['rows' => '2']) ?>
            <?= $form->field($model, 'state_id')->dropDownList($states, ['prompt' => 'Select', 'id' => 'state-id']) ?>
            <?php
            echo $form->field($model, 'city_id')->widget(DepDrop::classname(), [
                'options' => ['id' => 'city-id'],
                'pluginOptions' => [
                    'depends' => ['state-id'],
                    'placeholder' => 'Select...',
                    'url' => Url::to(['/registration/city-dropdown-by-state'])
                ]
            ]);
            ?>
            <?= $form->field($model, 'zipcode') ?>




</div>
