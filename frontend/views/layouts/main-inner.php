<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\widgets\ActiveForm;

AppAsset::register($this);
$session = Yii::$app->session;

$model = \yii\helpers\ArrayHelper::map(\common\models\Contents::find()->all(), 'page', 'content');
?>
<?php $this->beginPage()
?>
<!doctype html>
<html lang="en">
    <head>
        <title>My Visa - Professional webite For Visa  </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Resource style -->
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <link href="/css/animate.min.css" rel="stylesheet">

        <!-- favicon -->
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon"/>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet" type="text/css">
    </head>
    <body class="color-subpage">
        <?php $this->beginBody(); ?>
        <!--  Header -->
        <div class="header-before"><div class="container">
                <div class="row">
                    <div class="col-lg-11 col-md-9 col-sm-10 col-xs-12 slider-address">
                        <ul>
                            <li><i class="icon icon-phone"></i><?php echo $model['top_bar_contact_number']; ?></li>
                            <li><i class="icon icon-clock"></i><?php echo $model['top_bar_contact_email']; ?></li>
                            <li><i class="icon icon-mail"></i><?php echo $model['top_bar_opening_hours']; ?></li>
                        </ul>
                    </div>

                    <div class="col-lg-1 col-md-3 col-sm-2 col-xs-12 slider-social">
                        <ul>
                            <li><a href="https://twitter.com/myvisakwt" target="_blank"><i class="icon icon-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/Dubai-Visa-1516451458645786/?ref=bookmarks" target="_blank"><i class="icon icon-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/" target="_blank"><i class="icon icon-Instagram"></i></a></li>
                        </ul>
                    </div>

                </div></div>
        </div>

        <header class="inside-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-3 col-xs-6"> <div class="logo"><a href="/site/index"><img src="/img/logo.png" alt="Logo"></a></div> </div>
                    <div class="col-md-8 col-sm-9 col-xs-6"> 
                        <a id="menu-toggle" class="button dark" href="#"><i class="icon-menu"></i></a>
                        <nav id="navigation">
                            <ul id="main-menu">
                                <li><a href="/site/index" >Home</a></li>
                                <li><a href="/site/necessary-documents" >Necessary documents</a></li>
                                <li><a href="/site/contact" >Contact Us</a></li>
                                <?php
                                if (Yii::$app->user->isGuest && !$session->get('guest_id')) {
                                    echo '<li><a href="#" data-toggle="modal" data-target="#loginModal">Login</a></li>';
                                } else {
//                                    if (!$session->get('guest_id'))
//                                        echo '<li><a href="/site/add-user" >Add User</a></li>';
//
//
//                                    if ($session->get('guest_id'))
//                                        echo '<li><a href="/site/country-selection" >Country Selection</a></li>';

                                    if (!$session->get('guest_id'))
                                        echo '<li><a href="/site/logout-staff" data-method="post">Logout</a></li>';
                                    else
                                        echo '<li><a href="/site/logout-guest" >Logout</a></li>';
                                }
                                ?>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>

        </header>
        <!-- /Header -->

        <?= $content ?>

        <!-- Footer  -->
        <footer class="footer-section section">
            <div class="container">
                <div class="row text-center">
                    <div class="col-sm-12 col-xs-12">
                        <div class="footer-logo wow fadeInUp">
                            <p><img src="/img/logo-ftr.png" height="180" alt="Logo"></p>
                        </div>
                        <div class="copyright-info wow fadeInUp">
                            <p>© 2015 <span class="myvisa-theme">My<small>Visa</small></span>. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Login Modal -->
        <section id="modals">
            <!-- Login Modal -->
            <div class="modal login fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">

                            <div id="login-salect" >
                                <label>IAM</label> <br/>
                                <div class="styled">
                                    <select id="reportsSelect">
                                        <option value="staff-member" id="staff-member" >Staff Member</option>
                                        <option value="guest-user" id="guest-user"  selected="selected">Guest User</option>
                                    </select></div>
                            </div>

                            <div id="login-content" >
                                <!-- Start First Section -->
                                <button type="button" class="login-close" data-dismiss="modal"> </button>

                                <div id="guest-userDiv" style="display:block;">
                                    <?php
                                    $model = new common\models\GuestUsers();
                                    $form = ActiveForm::begin(['options' => [
                                                    'class' => 'FlowupLabels',
                                                    'id' => 'guestuser'
                                                ]
                                                , 'enableClientValidation' => false, 'action' => '/site/guest-login']);
                                    ?>

                                    <h6 class="login-title">I am a Guest User</h6>

                                    <div class='fl_wrap form-group'>
                                        <span class="icon icon-name"></span>
                                        <label class='fl_label' for='guestusers-fullname'>Full Name</label>
                                        <?= Html::activeTextInput($model, 'fullname', ['class' => 'fl_input']); ?>
                                    </div>

                                    <div class='fl_wrap form-group'>
                                        <span class="icon icon-kind-man"></span>
                                        <label class='fl_label' for='guestusers-email'>Email Address</label>
                                        <?= Html::activeTextInput($model, 'email', ['id' => 'label02', 'class' => 'fl_input']); ?>
                                    </div>

                                    <div class='fl_wrap form-group'>
                                        <span class="icon icon-kind-man"></span>
                                        <label class='fl_label' for='guestusers-phone_no'>Phone Number</label>
                                        <?= Html::activeTextInput($model, 'phone_no', ['id' => 'label03', 'class' => 'fl_input']); ?>
                                    </div>

                                    <button type="submit" class="btn btn-shutter login-but">Login</button>
                                    <?php ActiveForm::end(); ?>   
                                </div>

                                <!-- Start Second Section -->
                                <div id="staff-memberDiv" style="display:none;">
                                    <?php
                                    $model = new common\models\LoginForm();
                                    $form = ActiveForm::begin(['options' => [
                                                    'class' => 'FlowupLabels',
                                                    'id' => 'staffuser'
                                                ]
                                                , 'enableClientValidation' => false, 'action' => '/site/staff-login']);
                                    ?>
                                    <h6 class="login-title">I am a Staff Member</h6>

                                    <div class='fl_wrap form-group'>
                                        <span class="icon icon-name"></span>
                                        <label class='fl_label' for='loginform-username'>User Name</label>
                                        <?= Html::activeTextInput($model, 'username', ['class' => 'fl_input']); ?>
                                    </div>

                                    <div class='fl_wrap form-group'>
                                        <span class="icon icon-kind-man"></span>
                                        <label class='fl_label' for='loginform-password'>Password</label>
                                        <?= Html::activePasswordInput($model, 'password', ['class' => 'fl_input']); ?>
                                    </div>
                                    <button type="submit" class="btn login-but">Login</button>

                                    <?php ActiveForm::end(); ?>   
                                </div>
                            </div>     




                        </div>
                    </div>
                </div>
            </div>
            <!-- /Login Modal -->
        </section>
        <!-- Scroll To Top -->
        <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

        <script src="/js/jquery-1.11.3.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/wow.min.js"></script>
        <script src="/js/jquery.ajaxMailChimp.min.js"></script>
        <script src="/js/jquery.touchSwipe.min.js"></script>
        <script src="/js/jquery.swipebox.min.js"></script>
        <script src="/js/counts.js"></script>
        <script src="/js/jquery.countTo.js"></script>
        <script src="/js/jquery.smooth-scroll.js"></script>
        <script src="/js/main.js"></script>
        <script src="/js/jquery.typer.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/jquery.ui.fullscreen.js" type="text/javascript"></script>
        <script src="/js/fullscreen.js" type="text/javascript"></script>
        <script src="/js/jquery.FlowupLabels.js"></script>
        <!-- must have -->
        <script>
            ////// responsive MENU

            $(document).ready(function() {

                $('#menu-toggle').click(function() {
                    $('#main-menu').slideToggle(300);
                    return false;
                });

                $(window).resize(function() {
                    if ($(window).width() > 700) {
                        $('#main-menu').removeAttr('style');
                    }
                });

            });

        </script>
        <script>
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf("msie") != -1 || ua.indexOf("opera") != -1) {
                jQuery('body').css('overflow', 'hidden');
                jQuery('html').css('overflow', 'hidden');
            }
            jQuery(function() {
                jQuery('#fullscreen-slider').bannerscollection_zoominout({
                    skin: 'opportune',
                    responsive: true,
                    width: 1920,
                    height: 1200,
                    width100Proc: true,
                    height100Proc: true,
                    fadeSlides: 1,
                    showNavArrows: true,
                    showBottomNav: true,
                    autoHideBottomNav: true,
                    thumbsWrapperMarginTop: -55,
                    pauseOnMouseOver: false
                });

            });

            $(function() {
                $.fn.showField = function() {
                    var selectVal = document.getElementById(this.val() + 'Div');
                    return this.each(function() {
                        $(selectVal).show().siblings('div').hide();
                    });
                };
                $('select#reportsSelect').change(function() {
                    $(this).showField();
                });
            });


            ////// label focus

            (function() {
                $('.FlowupLabels').FlowupLabels({
                    /*
                     * These are all the default values
                     * You may exclude any/all of these options
                     * if you won't be changing them
                     */

                    // Handles the possibility of having input boxes prefilled on page load
                    feature_onInitLoad: true,
                    // Class when focusing an input
                    class_focused: 'focused',
                    // Class when an input has text entered
                    class_populated: 'populated'
                });
            })();

        </script> 
        <script src="/js/animation.js"></script>
    </body>
</html>
<?php $this->endPage() ?>
