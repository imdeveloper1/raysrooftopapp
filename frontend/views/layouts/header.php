<?php
use \frontend\components\Link;
use \yii\helpers\Html;
use Yii;


$session = Yii::$app->session;
if ($session->get('theme') == true) {

    $this->registerCssFile(Yii::$app->request->baseUrl . '/css/theme2.css', [
        'depends' => [\yii\web\YiiAsset::className()],
    ]);

} else {

    $this->registerCssFile(Yii::$app->request->baseUrl . '/css/theme.css', [
        'depends' => [\yii\web\YiiAsset::className()],
    ]);
}
?>

    <!-- Header Part Start Here-->
    <header>
        <div class="main">
            <div class="logo"><a href="<?= Link::getHome(); ?>" title="CSR"><img src="/image/csr_logo.png"
                                                                                 alt="CSR"></a></div>
            <div class="headpanel">
                <div class="top">
                    <div class="screenreader">
                        <div class="screen"><a href="TBD" title="Screen Reader Access">Screen Reader Access</a></div>
                        <div class="access"><span><a href="javascript:void(0)" title="+" class="js-font-increase">+</a></span>
                            <span class="active">
                                <a href="javascript:void(0)" title="A"
                                   class="js-font-normal">A</a></span> <span class="borderrnone"><a
                                    href="javascript:void(0)" title="-" class="js-font-decrease">-</a></span></div>
                        <div class="access2">
                            <a onclick="changeTheme('white');" href="javascript:void(0);" title="White">White</a>
                            <a onclick="changeTheme('black');" href="javascript:void(0);" title="Black">Black</a>
                            <!--                            <span class="accessvalue">B</span>-->
                        </div>
                    </div>
                    <div class="headerright">

                        <div class="search">
                        </div>
                        <!--                    <a href="TBD" title="Register" class="gapper">Register</a> <a href="TBD" title="Login">Login</a>-->
                    </div>
                </div>
                <nav id="menu">
                    <div class="abrir_menu"><span class="sprite" title="Menu"></span></div>
                    <div class="menu_aberto ">
                        <div id="navigator">
                            <ul class="nav">
                                <li class="<?= getActiveClass('site', 'index'); ?>" title="Home"><a
                                        href="<?= Link::getHome(); ?>" title="Home">Home</a></li>
                                <?php if (Yii::$app->user->isGuest) : ?>
                                    <li title="Corporate Registration"
                                        class="<?= getActiveClass('registration', 'corporate'); ?>">
                                        <a href="<?= Link::corporateRegistration(); ?>" title="Register">Register</a>
                                    </li>
                                    <li title="Login" class="<?= getActiveClass('site', 'login'); ?>">
                                        <a href="<?= Link::getLogin(); ?>" title="Login">Login</a>
                                    </li>
                                <?php else : ?>
                                    <li title="Post Project" class="<?= getActiveClass('project', 'create'); ?>">
                                        <a href="<?= Link::postProject(); ?>" title="Post Project">Project
                                            Information</a>
                                    </li>
                                    <li title="Logout" <?= getActiveClass('site', 'logout'); ?>><?= Html::a('logout (' . Yii::$app->user->identity->username . ' )', '/site/logout.html', ['data-method' => 'post']); ?>

                                    </li>
                                <?php endif; ?>

                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="clear"></div>
    </header>
    <!-- Header Part End Here-->
<?php
function getActiveClass($controller, $action)
{
    $activeController = Yii::$app->controller->id;
    $activeAction = Yii::$app->requestedAction->id;

    if (($activeController == $controller) && ($action == $activeAction)) {
        return 'active';
    }
}

?>