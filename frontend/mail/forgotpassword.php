<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2>Hi, your new password is : <?php echo $newpassword ?></h2>
<?= Html::a('Go to home page', Url::home('http')) ?>
