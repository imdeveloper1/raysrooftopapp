<?php
namespace frontend\components;

use kartik\helpers\Html;
use Yii;

/**
 * Created by PhpStorm.
 * User: ravi
 * Date: 5/10/15
 * Time: 1:14 PM
 */
class Link
{

    public static function getHome()
    {
        return Yii::$app->getHomeUrl();
    }

    public static function getAbout()
    {
        return Yii::$app->urlManager->createUrl(['site/about']);
    }

    public static function getLogin()
    {
        return Yii::$app->urlManager->createUrl(['site/login']);
    }

    public static function getLogout()
    {
        return Yii::$app->urlManager->createUrl(['site/logout'], ['data-method' => 'post']);
    }

    public static function corporateRegistration()
    {
        return Yii::$app->urlManager->createUrl(['registration/corporate']);
    }

    public static function postProject()
    {
        return Yii::$app->urlManager->createUrl(['project/create']);
    }
} 