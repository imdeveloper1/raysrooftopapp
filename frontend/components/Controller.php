<?php
/**
 * Created by PhpStorm.
 * User: ravi
 * Date: 5/10/15
 * Time: 12:09 PM
 */

namespace frontend\components;

class Controller extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        $cookies = \Yii::$app->response->cookies;

        if (!$cookies->has('blindness')) {
            $cookies->add(new \yii\web\Cookie([
                'name' => 'blindness',
                'value' => 'true',
            ]));
        }

        return parent::beforeAction($action);
    }
}