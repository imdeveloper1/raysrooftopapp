// JavaScript Document
/*Site Navigation Js Start Here*/
jQuery(document).ready(function($){
  $('.abrir_menu').click(function(){
    $("body").toggleClass('no_scroll');
  });
});
jQuery(document).ready(function($){
  $(function() {
    initDropDowns($("#menu"));
  });

  function initDropDowns(allMenus) {
    allMenus.children(".abrir_menu").on("click", function() {
      var thisTrigger = jQuery(this),
          thisMenu = thisTrigger.parent(),
          thisPanel = thisTrigger.next();

      if(thisMenu.hasClass("open")){
        thisMenu.removeClass("open");
        jQuery(document).off("click");                                 
        thisPanel.off("click");
      }
      else{	
        allMenus.removeClass("open");	
        thisMenu.addClass("open");
        jQuery(document).on("click", function() {
          allMenus.removeClass("open");
        });
        thisPanel.on("click", function(e) {
          e.stopPropagation();
        });
      }				
      return false;
    });
  }
});
jQuery(document).ready(function($){
  $(document).ready(function() {
    $(".nav li a").each(function() {
      if ($(this).next().length > 0) {
        $(this).addClass("parent");
      };
    })
  })
  $(function(){
    $(".nav li").unbind('mouseenter mouseleave');
    $(".nav li a.parent").unbind('click').bind('click', function(e) {
      // must be attached to anchor element to prevent bubbling
      e.preventDefault();
     $(this).parent("li").toggleClass("hover");
	  $(this).parent("li").toggleClass("active");
    });
  });
});
/*Header Search*/
$(document) .ready(function() {
	$(".searchshow") .click(function() {
		$(".searchbar") .show();
	});
	
	$("#herobanner") .click(function() {
		$(".searchbar") .hide();
	});
});
// Font Size
$(document).ready(function(touch){
$('header .access span').click(function(touch){
	$('header .access span.active').removeClass('active');
	$(this).addClass('active');
});
});	

$(document).ready(function(touch){
$('header .access2 a').click(function(touch){
	$('header .access2 a.active').removeClass('active');
	$(this).addClass('active');
});
});	

	
// top Slider
$('#sliderrun').slick({
        dots: false,
        infinite: true,
        speed: 500,
        arrows: false,
		slidesToShow: 1,
        slidesToScroll: 1,
       autoplay: true,
       autoplaySpeed: 5000,
		settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            },
    });
// Form 
$(document).ready(function() {
  $('form.material-form :input,form.material-form-2 :input').bind({
    focus: function() {
      $("label[for='" + this.id + "']").addClass("labelfocus");

    },
    blur: function() {
      if(!$(this).val()){$("label[for='" + this.id + "']").removeClass("labelfocus");}
    }
  });
  
  var fullInputs = $('input:text').filter(function() { return this.value !== ""; });

    fullInputs.each(function() {
      $("label[for='" + this.id + "']").addClass("labelfocus");
    });
  
});
// Project Page Select
$(document).ready(function(touch){
	$(".custom-select").each(function(touch){
		$(this).wrap("<span class='select-wrapper'></span>");
		$(this).after("<span class='holder'></span>");
	});
	$(".custom-select").change(function(touch){
		var selectedOption = $(this).find(":selected").text();
		$(this).next(".holder").text(selectedOption);
	}).trigger('change');
});

// Custom Check box
function setupLabel() {
   if ($('.label_check input').length) {
       $('.label_check ').each(function(){
           $(this).removeClass('c_on');
       });
       $('.label_check input:checked').each(function(){
           $(this).parent('div').addClass('c_on');
       });              
   };
   if ($('.label_radio input').length) {
       $('.label_radio').each(function(){
           $(this).removeClass('r_on');
       });
       $('.label_radio input:checked').each(function(){
           $(this).parent('div').addClass('r_on');
       });
   };
};
$(document).ready(function(){
   $('.chkhold').addClass('has-js');
   $('.label_check, .label_radio').click(function(){
       setupLabel();

   });
   setupLabel();
});



//Font Resizer
(function($) {
  function changeFont(fontSize) {
      return function() {
         $('html').css('font-size', fontSize + '%');
         sessionStorage.setItem('fSize', fontSize);
      }
    }
    var normalFont = changeFont(88),
    		mediumFont = changeFont(100),
    		largeFont  = changeFont(108);

    $('.js-font-decrease').on('click', function(){
      normalFont();
    });

    $('.js-font-normal').on('click', function(){
      mediumFont();
    });
    $('.js-font-increase').on('click', function(){
      largeFont();
    });

    if (sessionStorage.length !== 0) {
      $('html').css('font-size', sessionStorage.getItem('mediumFont') + '%');
    }
})(jQuery);

// Post Page- Multiple select dropdown
$(document).ready(function () {
    window.testSelAll = $('.testSelAll').SumoSelect({okCancelInMulti:true, selectAll:true });
});
