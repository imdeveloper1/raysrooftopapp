/**
 * Created by gspl on 7/10/15.
 */


function changeTheme(theme) {

    $.ajax({
        type: "POST",
        url: "/ajax/set-theme-flag.html",
        data: {theme: theme},
        success: function(data){

        }
    });
    location.reload();

}

function getCityByState(stateId){
    if(stateId) {
        $.ajax({
            type: "POST",
            url: "/ajax/get-city-drop-down-by-state.html",
            data: {stateId: stateId},
            success: function (data) {
                $("#userprofile-city_id").html(data);
            }
        });
    }
}

