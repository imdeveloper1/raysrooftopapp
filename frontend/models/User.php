<?php

namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $type
 * @property string $username
 * @property string $company_name
 * @property string $email
 * @property string $password
 * @property string $contact_info
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $company_type
 * @property string $website_url
 * @property string $payment_detail
 * @property string $status
 * @property string $role
 * @property integer $all_notification
 * @property integer $email_notification
 * @property integer $sms_notification
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 *
 * @property Profile $profile
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class User extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $password_repeat;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public static function tableName() {
        return 'user';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'username', 'email'], 'required'],
            //[['username'], 'string', 'max' => 25],
            ['email', 'email'],
          //  [['email'], 'string', 'max' => 255],
          //  [['password_hash', 'status', 'role'], 'string', 'max' => 50],
          //  [['contact_info', 'city', 'state', 'zipcode'], 'string', 'max' => 100],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
            [['phone_no'], 'string', 'max' => 500],
            [['auth_key'], 'string', 'max' => 32],
           // [['registration_ip'], 'string', 'max' => 45],            
            //[['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'username' => 'Username',
          //  'company_name' => 'Company Name',
            'email' => 'Email',
            'password' => 'Password',
            //'contact_info' => 'Contact Info',
            //'address' => 'Address',
            //'city' => 'City',
            //'state' => 'State',
            //'zipcode' => 'Zipcode',
            //'company_type' => 'Company Type',
            //'website_url' => 'Website Url',
            //'payment_detail' => 'Payment Detail',
            //'status' => 'Status',
            //'role' => 'Role',
            //'all_notification' => 'All Notification',
           // 'email_notification' => 'Email Notification',
           // 'sms_notification' => 'Sms Notification',
           // 'auth_key' => 'Auth Key',
           // 'confirmed_at' => 'Confirmed At',
            'email' => 'Email',
            //'blocked_at' => 'Blocked At',
           // 'registration_ip' => 'Registration Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
           // 'flags' => 'Flags',
         //   'devicetoken' => 'Devicetoken',
          //  'devicetype' => 'Devicetype'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts() {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens() {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }

    public function sendAndroid($registerid, $msg) {


        $APIKEY = Yii::$app->params['API_ACCESS_KEY'];
        $registrationIds = array($registerid);

// prep the bundle
//        $msg = array
//            (
//            'message' => 'here is a message. message',
//            'title' => 'This is a title. title',
//            'subtitle' => 'This is a subtitle. subtitle',
//            'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
//            'vibrate' => 1,
//            'sound' => 1,
//            'largeIcon' => 'large_icon',
//            'smallIcon' => 'small_icon'
//        );

        $fields = array
            (
            'registration_ids' => $registrationIds,
            'data' => $msg
        );

        $headers = array
            (
            'Authorization: key=' . $APIKEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        echo $result;
    }

}
