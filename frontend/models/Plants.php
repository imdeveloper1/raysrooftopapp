<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "plants".
 *
 * @property integer $id
 * @property string $name
 * @property string $zipcode
 * @property string $latitude
 * @property string $longitude
 */
class Plants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'zipcode', 'latitude', 'longitude'], 'required'],
            [['latitude', 'longitude'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['zipcode'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'zipcode' => 'Zipcode',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
