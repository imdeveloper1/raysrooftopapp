<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "details".
 *
 * @property integer $id
 * @property string $connection_type
 * @property string $contract_load_value
 * @property string $contract_load_unit
 * @property string $roof_type
 * @property string $roof_inclination
 * @property integer $roof_area
 * @property integer $monthly_unit
 * @property integer $monthly_bill
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Details extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['connection_type', 'contract_load_value', 'contract_load_unit', 'roof_type', 'roof_inclination', 'roof_area', 'monthly_unit', 'monthly_bill', 'created_at', 'updated_at'], 'required'],
            [['roof_area', 'monthly_unit', 'monthly_bill', 'created_at', 'updated_at'], 'integer'],
            [['status'], 'string'],
            [['connection_type', 'contract_load_value', 'roof_type', 'roof_inclination'], 'string', 'max' => 180],
            [['contract_load_unit'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'connection_type' => 'Connection Type',
            'contract_load_value' => 'Contract Load Value',
            'contract_load_unit' => 'Contract Load Unit',
            'roof_type' => 'Roof Type',
            'roof_inclination' => 'Roof Inclination',
            'roof_area' => 'Roof Area',
            'roof_area_unit' => 'Roof Area Unit',
            'monthly_unit' => 'Monthly Unit',
            'monthly_bill' => 'Monthly Bill',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
