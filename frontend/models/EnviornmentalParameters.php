<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "enviornmental_parameters".
 *
 * @property integer $id
 * @property integer $km
 * @property integer $co_produced
 * @property integer $fuel_saved
 * @property integer $trees_planted
 * @property integer $cars_removed
 */
class EnviornmentalParameters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enviornmental_parameters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['km', 'co_produced', 'fuel_saved', 'trees_planted', 'cars_removed'], 'required'],
            [['km', 'co_produced', 'fuel_saved', 'trees_planted', 'cars_removed'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'km' => 'Km',
            'co_produced' => 'Co Produced',
            'fuel_saved' => 'Fuel Saved',
            'trees_planted' => 'Trees Planted',
            'cars_removed' => 'Cars Removed',
        ];
    }
}
