<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "zipcodes".
 *
 * @property integer $id
 * @property string $city
 * @property string $zipcode
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Zipcodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zipcodes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'zipcode', 'status', 'created_at', 'updated_at'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['city', 'zipcode'], 'string', 'max' => 180]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'zipcode' => 'Zipcode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
